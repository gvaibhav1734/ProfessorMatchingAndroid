package nitk.ieee.professormatching;

import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private TextInputEditText mPasswordView;
    private ProgressBar mProgressBar;
    private View mLoginFormView;
    private TextInputLayout mPasswordViewTil;
    private TextInputLayout mUsernameViewTil;
    private TextInputLayout mEmailViewTil;
    private TextInputEditText mUsernameView;
    private Button mRegisterButton;
    private Button mEmailSignInButton;
    private boolean clicked = false;


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mUsernameView != null)
            outState.putString("username", mUsernameView.getText().toString());
        if (mEmailView != null)
            outState.putString("email", mEmailView.getText().toString());
        if (mPasswordView != null)
            outState.putString("password", mPasswordView.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null) {
            View splashScreen =
                    getLayoutInflater().inflate(R.layout.activity_main, null, false);
            setContentView(splashScreen);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    start(savedInstanceState);
                }
            }, 1000);
        } else {
            start(savedInstanceState);
        }
    }

    protected void start(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.login_toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        // Set up the login form.
        mEmailView = findViewById(R.id.login_tiet_email);
        populateAutoComplete();
        mPasswordViewTil = findViewById(R.id.login_til_password);
        mEmailViewTil = findViewById(R.id.login_til_email);
        mUsernameViewTil = findViewById(R.id.login_til_username);

        mPasswordView = findViewById(R.id.login_tiet_password);
        mUsernameView = findViewById(R.id.login_tiet_username);
        mLoginFormView = findViewById(R.id.login_cl_root);
        mProgressBar = findViewById(R.id.login_progress);

        if (savedInstanceState != null) {
            mUsernameView.setText(savedInstanceState.getString("username"));
            mEmailView.setText(savedInstanceState.getString("email"));
            mPasswordView.setText(savedInstanceState.getString("password"));
        }
        mRegisterButton = (Button) findViewById(R.id.login_btn_register);
        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(a);
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.login_btn_sign_in);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!clicked) {
                    toggleSignIn();
                    attemptLogin();
                }
            }
        });


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    mEmailSignInButton.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale
                    , Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        //Reset errors
        mUsernameView.setError(null);
        mPasswordView.setError(null);
        mEmailView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String username = mUsernameView.getText().toString();

        boolean cancel = false;

        // Check if user entered username
        if (TextUtils.isEmpty(username)) {
            cancel = true;
            mUsernameView.setError("Enter username");
        }

        // Check for a valid email address.
        else if (TextUtils.isEmpty(email)) {
            cancel = true;
            mEmailView.setError("Enter email");
        } else if (!isEmailValid(email)) {
            cancel = true;
            mEmailView.setError(getString(R.string.error_invalid_email));
        }

        //Check if user entered password
        else if (TextUtils.isEmpty(password)) {
            cancel = true;
            mPasswordView.setError("Enter password");
        }

        if (cancel) {
            // There was an error; don't attempt login.
            toggleSignIn();
        } else {
            StringRequest loginRequest = new StringRequest(
                    Request.Method.POST, getString(R.string.URL_LOG_IN)
                    , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String username = String.valueOf(object.get("username"));
                        String email = String.valueOf(object.get("email"));
                        String token = String.valueOf(object.get("token"));
                        String type = String.valueOf(object.get("type"));
                        User.getInstance(getApplicationContext())
                                .setData(username, email, token, type);

                        if (User.getInstance(getApplicationContext()).getType().equals("prof")) {
                            String editUrl = getString(R.string.URL_PROFESSOR_BASIC_INFO)
                                    + User.getInstance(getApplicationContext()).getUsername() + "/";
                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                                    Request.Method.GET, editUrl, null,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.d("OUTPUT", response.toString());
                                            ProfessorBasicInfo professorBasicInfo =
                                                    GsonHelper.getInstance(getApplicationContext())
                                                            .getGson().fromJson(response.toString(),
                                                            ProfessorBasicInfo.class);
                                            User.getInstance(getApplicationContext())
                                                    .getProfessor().basicInfo = professorBasicInfo;
                                            String url = getString(R.string.URL_PROFESSOR_DETAILED_INFO)
                                                    + User.getInstance(getApplicationContext()).getUsername()
                                                    + "/";
                                            JsonObjectRequest jsonObjectRequest1 =
                                                    new JsonObjectRequest(Request.Method.GET
                                                            , url
                                                            , null
                                                            , new Response.Listener<JSONObject>() {
                                                        @Override
                                                        public void onResponse(JSONObject response) {
                                                            Log.d("OUTPUT", response.toString());
                                                            ProfessorDetailedInfo professorDetailedInfo = GsonHelper.getInstance(getApplicationContext())
                                                                    .getGson().fromJson(response.toString(), ProfessorDetailedInfo.class);
                                                            User.getInstance(getApplicationContext()).getProfessor().detailInfo = professorDetailedInfo;
                                                            Intent a = new Intent(getApplicationContext()
                                                                    , DashboardActivity.class);
                                                            startActivity(a);
                                                        }
                                                    }
                                                            , new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            Intent a = new Intent(getApplicationContext()
                                                                    , ProfessorDetailInfoEntryActivity.class);
                                                            startActivity(a);
                                                        }
                                                    }) {
                                                        @Override
                                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                                            Map<String, String> params = new HashMap<String, String>();
                                                            params.put("Authorization", "Token "
                                                                    + User.getInstance(getApplicationContext()).getToken());
                                                            return params;
                                                        }
                                                    };
                                            VolleyHelper.getInstance(getApplicationContext())
                                                    .addToRequestQueue(jsonObjectRequest1);
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Intent a = new Intent(getApplicationContext()
                                                    , ProfessorBasicInfoEntryActivity.class);
                                            startActivity(a);
                                            Log.d("CONTROL", "UPDATE DATA PROFESSOR "
                                                    + error.getMessage());

                                        }
                                    }) {
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("Authorization", "Token "
                                            + User.getInstance(getApplicationContext()).getToken());
                                    return params;
                                }
                            };
                            VolleyHelper.getInstance(getApplicationContext())
                                    .addToRequestQueue(jsonObjectRequest);
                        } else {
                            String editUrl = getString(R.string.URL_STUDENT_BASIC_INFO)
                                    + User.getInstance(getApplicationContext()).getUsername() + "/";
                            JsonObjectRequest jsonObjectRequest =
                                    new JsonObjectRequest(Request.Method.GET
                                            , editUrl
                                            , null
                                            , new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.d("SUCCESS", response.toString());
                                            StudentBasicInfo studentBasicInfo =
                                                    GsonHelper.getInstance(getApplicationContext())
                                                            .getGson()
                                                            .fromJson(response.toString()
                                                                    , StudentBasicInfo.class);
                                            User.getInstance(getApplicationContext())
                                                    .getStudent()
                                                    .studentBasicInfo = studentBasicInfo;
                                            String editUrl = getString(R.string.URL_STUDENT_DETAILED_INFO) +
                                                    User.getInstance(getApplicationContext()).getUsername() + "/";
                                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                                                    Request.Method.GET
                                                    , editUrl
                                                    , null
                                                    , new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    try {
                                                        //#SensitiveLog
                                                        Log.d("SUCCESS", response.toString());
                                                        if (response.get("publications").equals(""))
                                                            response.remove("publications");
                                                        if (response.get("certificates").equals(""))
                                                            response.remove("certificates");
                                                        if (response.get("personalProjects").equals(""))
                                                            response.remove("personalProjects");
                                                        if (response.get("skillsInterest").equals(""))
                                                            response.remove("skillsInterest");
                                                        if (response.get("areas").equals(""))
                                                            response.remove("areas");
                                                        if (response.get("hobbies").equals(""))
                                                            response.remove("hobbies");
                                                        if (response.get("workEx").equals(""))
                                                            response.remove("workEx");
                                                        response.remove("skillsInterest");
                                                        String serverResponse = response.toString();
                                                        serverResponse = serverResponse.replace("\\\"", "\"");
                                                        serverResponse = serverResponse.replace("\"[", "[");
                                                        serverResponse = serverResponse.replace("]\"", "]");
                                                        response =
                                                                (JSONObject) new JSONTokener(serverResponse).nextValue();
                                                        //#SensitiveLog
                                                        Log.d("SUCCESS", response.toString());
                                                        StudentDetailInfo studentDetailedInfo =
                                                                GsonHelper.getInstance(getApplicationContext())
                                                                        .getGson()
                                                                        .fromJson(response.toString()
                                                                                , StudentDetailInfo.class);
                                                        User.getInstance(getApplicationContext())
                                                                .getStudent()
                                                                .studentDetailInfo = studentDetailedInfo;
                                                        Intent a = new Intent(getApplicationContext(), DashboardActivity.class);
                                                        startActivity(a);
                                                    } catch (Exception e) {
                                                        Log.d("ERROR", "STUDENT DETAIL INFO RESPONSE FAILURE "
                                                                + e.getMessage());
                                                    }
                                                }
                                            },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            Intent a = new Intent(getApplicationContext(), StudentInfoEntryActivity.class);
                                                            startActivity(a);
                                                        }
                                                    }) {

                                                @Override
                                                public Map<String, String> getHeaders() {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("Authorization",
                                                            "Token " + User.getInstance(getApplicationContext()).getToken());
                                                    return params;
                                                }

                                            };
                                            VolleyHelper.getInstance(getApplicationContext())
                                                    .addToRequestQueue(jsonObjectRequest);
                                        }
                                    }
                                            , new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Intent a = new Intent(getApplicationContext()
                                                    , StudentInfoEntryActivity.class);
                                            startActivity(a);
                                            Log.d("CONTROL", "UPDATE DATA PROFESSOR "
                                                    + error.getMessage());

                                        }
                                    }) {
                                        @Override
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            Map<String, String> params = new HashMap<String, String>();
                                            params.put("Authorization", "Token " + User.
                                                    getInstance(getApplicationContext()).getToken());
                                            return params;
                                        }
                                    };
                            VolleyHelper.getInstance(getApplicationContext())
                                    .addToRequestQueue(jsonObjectRequest);
                        }
                    } catch (org.json.JSONException e) {
                        Log.d("ERROR", "LOGIN GsonHelper OBJECT RESPONSE FAILURE");
                    }
                }
            }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("ERROR", "USER LOGIN FAILURE " + error.getMessage());
                    toggleSignIn();
                    Snackbar.make(mLoginFormView, "Username/Password Incorrect", Snackbar.LENGTH_SHORT)
                            .show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", email);
                    params.put("username", username);
                    params.put("password", password);

                    return params;
                }

            };
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(loginRequest);
        }
    }

    private void toggleSignIn() {
        if (clicked) {
            mProgressBar.setVisibility(View.GONE);
            mRegisterButton.setVisibility(View.VISIBLE);
            mEmailSignInButton.setVisibility(View.VISIBLE);
            clicked = false;
        } else {
            clicked = true;
            mProgressBar.setVisibility(View.VISIBLE);
            mRegisterButton.setVisibility(View.GONE);
            mEmailSignInButton.setVisibility(View.GONE);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace regex with a better one after looking up RFC882 whatever the hell that is!
        boolean check = email.matches("^.*?@gmail.com$");
        return true;
    }

    private boolean isPasswordValid(String password) {
        return true;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }
}


