package nitk.ieee.professormatching;

import java.util.List;

/**
 * Modal class to store detail of Professor
 */
public class Professor {
    ProfessorBasicInfo basicInfo;
    ProfessorDetailedInfo detailInfo;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

class ProfessorBasicInfo {

    ProfessorBasicInfo() {

    }

    //Basic Info
    private String firstName, lastName, gender, mobileNo, email;
    private int age;
    private String city, state, country;
    private String institute, department;
    private String areas, websiteLinks;

    //Getters and Setters.
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getWebsiteLinks() {
        return websiteLinks;
    }

    public void setWebsiteLinks(String websiteLinks) {
        this.websiteLinks = websiteLinks;
    }

    public String getAreas() {
        return areas;
    }

    public void setAreas(String areas) {
        this.areas = areas;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

class ProfessorDetailedInfo {
    ProfessorDetailedInfo() {

    }

    // Detailed Info
    private int minWorkEx;
    private int minYearOfStudy;
    private float minCgpa;
    private String branch, keywords, areas, selectedStudents;

    public int getMinWorkEx() {
        return minWorkEx;
    }

    public void setMinWorkEx(int minWorkEx) {
        this.minWorkEx = minWorkEx;
    }

    public int getMinYearOfStudy() {
        return minYearOfStudy;
    }

    public void setMinYearOfStudy(int minYearOfStudy) {
        this.minYearOfStudy = minYearOfStudy;
    }

    public float getMinCgpa() {
        return minCgpa;
    }

    public void setMinCgpa(float minCgpa) {
        this.minCgpa = minCgpa;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getAreas() {
        return areas;
    }

    public void setAreas(String areas) {
        this.areas = areas;
    }

    public String getSelectedStudents() {
        return selectedStudents;
    }

    public void setSelectedStudents(String selectedStudents) {
        this.selectedStudents = selectedStudents;
    }
}
