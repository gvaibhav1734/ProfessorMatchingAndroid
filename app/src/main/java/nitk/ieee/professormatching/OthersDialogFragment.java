package nitk.ieee.professormatching;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class OthersDialogFragment extends DialogFragment {
    private static final String TAG = "OthersDialog";
    private String hobbies, area, keyword;
    private boolean editable;
    private int layouts[] = {
            R.layout.dialog_others_areas,
            R.layout.dialog_others_keywords,
            R.layout.dialog_others_hobbies
    };
    private int itemPosition;
    private Button addSaveButton;
    private RelativeLayout deleteButton;
    private ViewPager viewPager;
    private OthersRecyclerViewAdapter othersRecyclerViewAdapter;
    private EditText areas, keywords;
    private EditText hobby;
    private Bundle savedInstanceState;

    public OthersDialogFragment() {
    }

    static OthersDialogFragment newInstance(String input, int type, boolean editable, int itemPosition) {
        if (type == OthersRecyclerViewAdapter.AREAS) {
            Bundle bundle = new Bundle();
            bundle.putString("area", input);
            bundle.putBoolean("editable", editable);
            bundle.putInt("position", itemPosition);
            OthersDialogFragment othersDialogFragment = new OthersDialogFragment();
            othersDialogFragment.setArguments(bundle);
            return othersDialogFragment;
        } else if (type == OthersRecyclerViewAdapter.KEYWORDS) {
            Bundle bundle = new Bundle();
            bundle.putString("keyword", input);
            bundle.putBoolean("editable", editable);
            bundle.putInt("position", itemPosition);
            OthersDialogFragment othersDialogFragment = new OthersDialogFragment();
            othersDialogFragment.setArguments(bundle);
            return othersDialogFragment;
        } else if (type == OthersRecyclerViewAdapter.HOBBIES) {
            Bundle bundle = new Bundle();
            bundle.putString("hobbies", input);
            bundle.putBoolean("editable", editable);
            bundle.putInt("position", itemPosition);
            OthersDialogFragment othersDialogFragment = new OthersDialogFragment();
            othersDialogFragment.setArguments(bundle);
            return othersDialogFragment;
        } else {
            return null;
        }
    }

    public void setAdapter(OthersRecyclerViewAdapter otherRecyclerViewAdapter) {
        this.othersRecyclerViewAdapter = otherRecyclerViewAdapter;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "Saving instance state");
        super.onSaveInstanceState(outState);
        if (hobby != null)
            outState.putString("hobby", hobby.getText().toString());
        if (areas != null)
            outState.putString("area", areas.getText().toString());
        if (keywords != null)
            outState.putString("keyword", keywords.getText().toString());
    }

    static OthersDialogFragment newInstance(boolean editable) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("editable", editable);
        OthersDialogFragment othersDialogFragment = new OthersDialogFragment();
        othersDialogFragment.setArguments(bundle);
        return othersDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "FRAGMENT");
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.get("area") != null) {
                area = bundle.getString("area");
                editable = bundle.getBoolean("editable");
                itemPosition = bundle.getInt("position");
                layouts = new int[1];
                layouts[0] = R.layout.dialog_others_areas;
            } else if (bundle.get("keyword") != null) {
                keyword = bundle.getString("keyword");
                editable = bundle.getBoolean("editable");
                itemPosition = bundle.getInt("position");
                layouts = new int[1];
                layouts[0] = R.layout.dialog_others_keywords;
            } else if (bundle.get("hobbies") != null) {
                hobbies = bundle.getString("hobbies");
                editable = bundle.getBoolean("editable");
                itemPosition = bundle.getInt("position");
                layouts = new int[1];
                layouts[0] = R.layout.dialog_others_hobbies;
            } else {
                editable = bundle.getBoolean("editable");
            }
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.dialog_others, container, false);
        viewPager = rootView.findViewById(R.id.dialog_others_vp);
        TabLayout tabLayout = rootView.findViewById(R.id.dialog_others_tl);
        addSaveButton = rootView.findViewById(R.id.dialog_others_btn_add_save);
        deleteButton = rootView.findViewById(R.id.dialog_others_delete);
        if (area != null || keyword != null || hobbies != null) {
            tabLayout.setVisibility(View.GONE);
            if (editable)
                deleteButton.setVisibility(View.VISIBLE);
        }
        if (!editable) {
            deleteButton.setVisibility(View.GONE);
            addSaveButton.setVisibility(View.GONE);
        }
        viewPager.setAdapter(new OthersDialogAdapter());
        if (savedInstanceState != null)
            this.savedInstanceState = savedInstanceState;
        tabLayout.setupWithViewPager(viewPager);
        return rootView;
    }

    class OthersDialogAdapter extends PagerAdapter {
        private static final String TAG = "OthersAdapter";
        String titles[] = {"Areas", "Keywords", "Hobbies"};

        OthersDialogAdapter() {

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup rootViewDialog = (ViewGroup) inflater.inflate(layouts[position], container, false);
            container.addView(rootViewDialog);

            Log.d(TAG, "Initializing fields");
            hobby = getDialog().findViewById(R.id.dialog_hobbies_hobby);
            areas = getDialog().findViewById(R.id.dialog_skills_area);
            keywords = getDialog().findViewById(R.id.dialog_skills_keyword);

            if (savedInstanceState != null) {
                Log.d(TAG, "Creating from savedInstanceState");
                if (areas != null)
                    areas.setText(savedInstanceState.getString("area"));
                if (keywords != null)
                    keywords.setText(savedInstanceState.getString("keyword"));
                if (hobby != null)
                    hobby.setText(savedInstanceState.getString("hobby"));

                if (getActivity() instanceof StudentInfoEntryActivity)
                    othersRecyclerViewAdapter = ((StudentInfoEntryActivity) getActivity())
                            .studentDetailEntryPagerAdapter
                            .mOthersRecyclerViewAdapter;
            }
            if (area != null) {
                areas.setText(area);
                if (editable) {
                    addSaveButton.setText("SAVE");
                    addSaveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String area = areas.getText().toString();
                            othersRecyclerViewAdapter.changeItemAt(
                                    itemPosition,
                                    area,
                                    OthersRecyclerViewAdapter.AREAS
                            );
                            getDialog().dismiss();
                        }
                    });
                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            othersRecyclerViewAdapter.deleteAreasAt(itemPosition);
                            getDialog().dismiss();
                        }
                    });
                }
                if (!editable) {
                    areas.setFocusable(editable);
                    areas.setCursorVisible(editable);
                    areas.setBackgroundColor(Color.TRANSPARENT);
                }
            } else if (keyword != null) {
                keywords.setText(keyword);
                if (editable) {
                    addSaveButton.setText("SAVE");
                    addSaveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String keyword = keywords.getText().toString();
                            othersRecyclerViewAdapter.changeItemAt(
                                    itemPosition,
                                    keyword,
                                    OthersRecyclerViewAdapter.KEYWORDS
                            );
                            getDialog().dismiss();
                        }
                    });

                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            othersRecyclerViewAdapter.deleteKeywordsAt(itemPosition);
                            getDialog().dismiss();
                        }
                    });
                }
                if (!editable) {
                    keywords.setFocusable(editable);
                    keywords.setCursorVisible(editable);
                    keywords.setBackgroundColor(Color.TRANSPARENT);
                }
            } else if (hobbies != null) {
                hobby.setText(hobbies);
                if (editable) {
                    addSaveButton.setText("SAVE");
                    addSaveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String hobbies = hobby.getText().toString();
                            othersRecyclerViewAdapter.changeItemAt(
                                    itemPosition,
                                    hobbies,
                                    OthersRecyclerViewAdapter.HOBBIES
                            );
                            getDialog().dismiss();
                        }
                    });

                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            othersRecyclerViewAdapter.deleteHobbiesAt(itemPosition);
                            getDialog().dismiss();
                        }
                    });
                }
                if (!editable) {
                    hobby.setFocusable(editable);
                    hobby.setCursorVisible(editable);
                    hobby.setBackgroundColor(Color.TRANSPARENT);
                }
            } else {
                //Add click listener to common button.
                addSaveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewPager.getCurrentItem() == OthersRecyclerViewAdapter.AREAS) {
                            String area = areas.getText().toString();
                            othersRecyclerViewAdapter.insertItem(area,
                                    OthersRecyclerViewAdapter.AREAS);
                            areas.setText("");
                        } else if (viewPager.getCurrentItem() == OthersRecyclerViewAdapter.KEYWORDS) {
                            String keyword = keywords.getText().toString();
                            othersRecyclerViewAdapter.insertItem(keyword,
                                    OthersRecyclerViewAdapter.KEYWORDS);
                            keywords.setText("");
                        } else if (viewPager.getCurrentItem() == OthersRecyclerViewAdapter.HOBBIES) {
                            String hobbies = hobby.getText().toString();
                            othersRecyclerViewAdapter.insertItem(hobbies,
                                    OthersRecyclerViewAdapter.HOBBIES);
                            hobby.setText("");
                        }
                    }
                });
            }
            return rootViewDialog;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        List<String> makeList(String input) {
            StringTokenizer stringTokenizer = new StringTokenizer(input, ",");
            List<String> output = new ArrayList<>();
            while (stringTokenizer.hasMoreElements()) {
                String s = stringTokenizer.nextToken();
                if (!s.equals(""))
                    output.add(s);
            }
            return output;
        }
    }
}