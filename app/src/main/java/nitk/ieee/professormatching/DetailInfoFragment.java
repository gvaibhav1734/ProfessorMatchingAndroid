package nitk.ieee.professormatching;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailInfoFragment extends Fragment {

    private static TextView minWork;
    private static TextView minStudy;
    private static TextView minCGPA;
    private static TextView branch;
    private LinearLayout editLayout, submitLayout;
    private Button submit, cancel;


    public DetailInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("minWork", minWork.getText().toString());
        savedInstanceState.putString("minStudy", minStudy.getText().toString());
        savedInstanceState.putString("minCGPA", minCGPA.getText().toString());
        savedInstanceState.putString("branch", branch.getText().toString());

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detailedinfo, container, false);

        minWork = rootView.findViewById(R.id.minWorkEx);
        minStudy = rootView.findViewById(R.id.minYrStudy);
        minCGPA = rootView.findViewById(R.id.minCGPA);
        branch = rootView.findViewById(R.id.branch);
        noneditable();

        if (savedInstanceState == null) {
            minWork.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().detailInfo.getMinWorkEx()));
            minStudy.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().detailInfo.getMinYearOfStudy()));
            minCGPA.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().detailInfo.getMinCgpa()));
            if (User.getInstance(getActivity()).getProfessor().detailInfo.getBranch() != null)
                branch.setText(User.getInstance(getActivity()).getProfessor().detailInfo.getBranch()
                        .replace("[", "")
                        .replace("]", "")
                        .replace("\"", ""));
        } else {
            minWork.setText(savedInstanceState.getString("minWork"));
            minStudy.setText(savedInstanceState.getString("minStudy"));
            minCGPA.setText(savedInstanceState.getString("minCGPA"));
            branch.setText(savedInstanceState.getString("branch"));
        }
        return rootView;
    }

    public void editable() {
        minWork.setCursorVisible(true);
        minWork.setFocusable(true);
        minWork.setFocusableInTouchMode(true);
        minCGPA.setCursorVisible(true);
        minCGPA.setFocusable(true);
        minCGPA.setFocusableInTouchMode(true);
        minStudy.setCursorVisible(true);
        minStudy.setFocusable(true);
        minStudy.setFocusableInTouchMode(true);
        branch.setCursorVisible(true);
        branch.setFocusable(true);
        branch.setFocusableInTouchMode(true);
    }

    public void noneditable() {
        minWork.setCursorVisible(false);
        minWork.setFocusable(false);
        minCGPA.setCursorVisible(false);
        minCGPA.setFocusable(false);
        minStudy.setCursorVisible(false);
        minStudy.setFocusable(false);
        branch.setCursorVisible(false);
        branch.setFocusable(false);
    }

    public void update() {

        String areasString, keywordsString, branchString;
        boolean makeServerRequest = true;
        String minWorkExperienceString = minWork.getText().toString();
        String minCgpaString = minCGPA.getText().toString();
        String minYearOfStudyString = minStudy.getText().toString();
        //String areasStringIntermediate = areas.getText().toString();
        //String keywordsStringIntermediate = keywords.getText().toString();
        String branchStringIntermediate = branch.getText().toString();
        //Array Handling
        {
            //JSONArray areasArray = new JSONArray(), keywordsArray = new JSONArray(),
            JSONArray branchArray = new JSONArray();
            /*StringTokenizer areasTokenizer = new StringTokenizer(areasStringIntermediate, ",");
            while (areasTokenizer.hasMoreTokens())
                areasArray.put(areasTokenizer.nextToken());
            StringTokenizer keywordsTokenizer = new StringTokenizer(keywordsStringIntermediate, ",");
            while (keywordsTokenizer.hasMoreTokens())
                keywordsArray.put(keywordsTokenizer.nextToken());*/
            StringTokenizer branchTokenizer = new StringTokenizer(branchStringIntermediate, ",");
            while (branchTokenizer.hasMoreTokens())
                branchArray.put(branchTokenizer.nextToken());
            //areasString = areasArray.toString();
            //keywordsString = keywordsArray.toString();
            branchString = branchArray.toString();
        }
        //Necessary Checks to determine whether the information should be passed to server.
        {
           /* if (areasString.equals(""))
                areasString = null;
            if (keywordsString.equals(""))
                keywordsString = null;*/
            if (branchString.equals(""))
                branchString = null;
            if (minWorkExperienceString.equals(""))
                makeServerRequest = false;
            if (minCgpaString.equals(""))
                makeServerRequest = false;
            if (minYearOfStudyString.equals(""))
                makeServerRequest = false;
            //TODO : More checks and display appropriate error messages
        }
        if (makeServerRequest) {
            JSONObject json = new JSONObject();
            try {
                json.put("username", User.getInstance(getContext()).getUsername());
                json.put("minCgpa", minCgpaString);
                json.put("minWorkEx", minWorkExperienceString);
                json.put("minYearOfStudy", minYearOfStudyString);
                //json.put("areas", areasString);
                //json.put("keywords", keywordsString);
                json.put("branch", branchString);
            } catch (Exception e) {
                Log.d("ERROR", "PROFESSOR DETAIL INFO GsonHelper CREATION FAILURE");
            }
            //#SensitiveLog
            Log.d("OUTPUT ", json.toString());
            Log.d("OUTPUT Token ", User.getInstance(getContext()).getToken());
            String editUrl = getString(R.string.URL_PROFESSOR_DETAILED_INFO) +
                    User.getInstance(getContext()).getUsername() + "/";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.PUT, editUrl, json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //#SensitiveLog
                                Log.d("OUTPUT", response.toString());
                                ProfessorDetailedInfo professorDetailedInfo = GsonHelper.getInstance(getContext())
                                        .getGson().fromJson(response.toString(), ProfessorDetailedInfo.class);
                                User.getInstance(getContext()).getProfessor().detailInfo = professorDetailedInfo;
                                Intent a = new Intent(getContext(), DashboardActivity.class);
                                startActivity(a);
                            } catch (Exception e) {
                                Log.d("ERROR", "PROFESSOR DETAIL INFO CRUD RESPONSE FAILURE" + e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("ERROR", "PROFESSOR DETAIL INFO CRUD UPDATE FAILURE " + error.getMessage());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "Token " + User.
                            getInstance(getContext()).getToken());
                    return params;
                }

            };
            VolleyHelper.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
        }
    }

    public void initialize() {
        minWork.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().detailInfo.getMinWorkEx()));
        minStudy.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().detailInfo.getMinYearOfStudy()));
        minCGPA.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().detailInfo.getMinCgpa()));
        if (User.getInstance(getActivity()).getProfessor().detailInfo.getBranch() != null)
            branch.setText(User.getInstance(getActivity()).getProfessor().detailInfo.getBranch()
                    .replace("[", "")
                    .replace("]", "")
                    .replace("\"", ""));

    }
}


