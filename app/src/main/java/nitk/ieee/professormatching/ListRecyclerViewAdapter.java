package nitk.ieee.professormatching;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * CustomAdapter serves to configure and display each item in a list effectively
 * using layout_list_view_item as its parent layout.
 */
class ListRecyclerViewAdapter extends RecyclerView.Adapter<ListRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "ListViewAdapter";
    private List<Student> students = new ArrayList<>();
    private Context context;
    private static final int ALPHA_ANIMATION_TIME = 200;
    final static int ALL = 1;
    final static int SELECTED = 2;
    final static int UNSELECTED = 0;
    private int identifier;
    private ListContainerActivity activity;

    public ListRecyclerViewAdapter(Context context, int identifier) {
        this.context = context;
        this.identifier = identifier;
        activity = (ListContainerActivity) context;
    }

    public void addStudent(Student student) {
        if (identifier == SELECTED && student.isSelected() && students.contains(student))
            return;
        Log.d(TAG, "" + identifier);
        students.add(student);
        notifyItemInserted(students.size() - 1);
    }

    public void removeStudent(Student student) {
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getUsername().equals(student.getUsername())) {
                students.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public void setList(List<Student> students) {
        this.students = students;
    }

    public List<Student> getList() {
        return students;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (students.get(holder.getAdapterPosition()).isSelected())
            holder.select.setImageResource(R.drawable.ic_selected);
        else
            holder.select.setImageResource(R.drawable.ic_not_selected);

        holder.name.setText(students.get(position).getUsername());
        holder.city.setText(String.valueOf(students.get(position).studentBasicInfo.getCity()));
        holder.state.setText(String.valueOf(students.get(position).studentBasicInfo.getState()));
        GlideApp.with(holder.itemView)
                .load(students.get(position).studentBasicInfo.getProfileImage())
                .placeholder(R.drawable.heisenberg)
                .into(holder.profileImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent displayUserProfile = new Intent(context, StudentInfoEntryActivity.class);
                displayUserProfile.putExtra("username"
                        , students.get(holder.getAdapterPosition()).getUsername());
                displayUserProfile.putExtra("allowEdit", false);
                context.startActivity(displayUserProfile);
            }
        });
        holder.select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To prevent double clicks
                v.setClickable(false);
                holder.itemView.setClickable(false);
                //Depending on whether the student is present in selected list or not
                //changes are carried out to all,unselected and selected list.
                if (students.get(holder.getAdapterPosition()).isSelected()) {
                    students.get(holder.getAdapterPosition()).setSelected(false);
                    holder.select.setImageResource(R.drawable.ic_not_selected);
                    activity.mUnselectedAdapter
                            .addStudent(students.get(holder.getAdapterPosition()));
                    activity.mSelectedAdapter
                            .removeStudent(students.get(holder.getAdapterPosition()));
                    if (identifier == ALL)
                        activity.mAllAdapter.notifyItemChanged(holder.getAdapterPosition());
                    else
                        activity.mAllAdapter.notifyDataSetChanged();
                } else {
                    students.get(holder.getAdapterPosition()).setSelected(true);
                    activity.mSelectedAdapter
                            .addStudent(students.get(holder.getAdapterPosition()));
                    holder.select.setImageResource(R.drawable.ic_selected);
                    activity.mUnselectedAdapter
                            .removeStudent(students.get(holder.getAdapterPosition()));
                    if (identifier == ALL)
                        activity.mAllAdapter.notifyItemChanged(holder.getAdapterPosition());
                    else
                        activity.mAllAdapter.notifyDataSetChanged();
                }
            }
        });
        Log.i("LISTCONTAINER", students.get(position).getUsername());
        setAnimation(holder.itemView);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.layout_list_view_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView name, city, state;
        CircleImageView profileImage;
        ConstraintLayout listUser;
        ImageView select;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.list_container_item_tv_name);
            city = itemView.findViewById(R.id.list_container_item_tv_institute);
            state = itemView.findViewById(R.id.list_container_item_tv_cgpa);
            listUser = itemView.findViewById(R.id.list_container_item_cl_root);
            select = itemView.findViewById(R.id.list_container_item_btn);
            profileImage = itemView.findViewById(R.id.list_container_item_civ_profile_image);
        }
    }

    void setAnimation(View itemView) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
        alphaAnimation.setDuration(250);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(alphaAnimation);
        itemView.setAnimation(animationSet);
        itemView.animate();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return students.size();
    }
}
