package nitk.ieee.professormatching;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


/**
 * A simple {@link Fragment} subclass.
 */
public class BasicinfoFragment extends Fragment {

    private EditText firstName, lastName, age, telephone, institute, city, state, username, websiteLinks, department, gender;

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("firstName", firstName.getText().toString());
        savedInstanceState.putString("lastName", lastName.getText().toString());
        savedInstanceState.putString("age", age.getText().toString());
        savedInstanceState.putString("telephone", telephone.getText().toString());
        savedInstanceState.putString("institute", institute.getText().toString());
        savedInstanceState.putString("city", city.getText().toString());
        savedInstanceState.putString("state", state.getText().toString());
        savedInstanceState.putString("username", username.getText().toString());
        savedInstanceState.putString("websiteLinks", websiteLinks.getText().toString());
        savedInstanceState.putString("department", department.getText().toString());
        savedInstanceState.putString("gender", gender.getText().toString());

        super.onSaveInstanceState(savedInstanceState);
    }


    public BasicinfoFragment() {
        // Required empty public constructor
    }


    /*@Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_basicinfo, container, false);

        username = rootView.findViewById(R.id.user_profile_tv_username);
        firstName = rootView.findViewById(R.id.user_profile_tv_firstname);
        lastName = rootView.findViewById(R.id.user_profile_lastname);
        age = rootView.findViewById(R.id.age);
        telephone = rootView.findViewById(R.id.phone_no);
        institute = rootView.findViewById(R.id.professor_basic_info_tiet_institute);
        city = rootView.findViewById(R.id.user_profile_tv_city);
        state = rootView.findViewById(R.id.user_profile_tv_state);
        gender = rootView.findViewById(R.id.user_profile_tv_gender);
        department = rootView.findViewById(R.id.user_profile_department);
        websiteLinks = rootView.findViewById(R.id.user_profile_website);
        nonEditable();

        if (savedInstanceState == null) {
            username.setText(User.getInstance(getActivity()).getUsername());
            firstName.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getFirstName());
            lastName.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getLastName());
            age.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().basicInfo.getAge()));
            telephone.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getMobileNo());
            institute.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getInstitute());
            city.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getCity());
            state.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getState());
            gender.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getGender());
            department.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getDepartment());
            if (User.getInstance(getActivity()).getProfessor().basicInfo.getWebsiteLinks() != null)
                websiteLinks.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getWebsiteLinks()
                        .replace("[", "")
                        .replace("]", "")
                        .replace("\"", ""));
        } else {
            firstName.setText(savedInstanceState.getString("firstName"));
            lastName.setText(savedInstanceState.getString("lastName"));
            age.setText(savedInstanceState.getString("age"));
            telephone.setText(savedInstanceState.getString("telephone"));
            institute.setText(savedInstanceState.getString("institute"));
            city.setText(savedInstanceState.getString("city"));
            state.setText(savedInstanceState.getString("state"));
            gender.setText(savedInstanceState.getString("gender"));
            department.setText(savedInstanceState.getString("department"));
            websiteLinks.setText(savedInstanceState.getString("websiteLinks"));
            username.setText(savedInstanceState.getString("username"));
        }

      /*  editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editLayout.setVisibility(View.GONE);
                submitLayout.setVisibility(View.VISIBLE);

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        submitLayout.setVisibility(View.GONE);
                        editLayout.setVisibility(View.VISIBLE);
                    }
                });

            }
        });*/
        return rootView;
    }

    public void editable() {
        firstName.setFocusable(true);
        firstName.setCursorVisible(true);
        firstName.setFocusableInTouchMode(true);
        lastName.setFocusable(true);
        lastName.setCursorVisible(true);
        lastName.setFocusableInTouchMode(true);
        age.setFocusable(true);
        age.setCursorVisible(true);
        age.setFocusableInTouchMode(true);
        telephone.setFocusable(true);
        telephone.setCursorVisible(true);
        telephone.setFocusableInTouchMode(true);
        institute.setFocusable(true);
        institute.setCursorVisible(true);
        institute.setFocusableInTouchMode(true);
        state.setFocusable(true);
        state.setCursorVisible(true);
        state.setFocusableInTouchMode(true);
        city.setFocusable(true);
        city.setCursorVisible(true);
        city.setFocusableInTouchMode(true);
        websiteLinks.setFocusable(true);
        websiteLinks.setCursorVisible(true);
        websiteLinks.setFocusableInTouchMode(true);
        department.setCursorVisible(true);
        department.setFocusableInTouchMode(true);
        department.setFocusable(true);
    }

    public void nonEditable() {
        firstName.setFocusable(false);
        firstName.setCursorVisible(false);
        lastName.setFocusable(false);
        lastName.setCursorVisible(false);
        age.setFocusable(false);
        age.setCursorVisible(false);
        city.setFocusable(false);
        city.setCursorVisible(false);
        state.setFocusable(false);
        state.setCursorVisible(false);
        institute.setFocusable(false);
        institute.setCursorVisible(false);
        telephone.setFocusable(false);
        telephone.setCursorVisible(false);
        websiteLinks.setFocusable(false);
        websiteLinks.setCursorVisible(false);
        department.setFocusable(false);
        department.setCursorVisible(false);
    }

    public void update() {
        boolean makeServerRequest = true;
        String areasString, websiteLinksString;
        String firstNameString = firstName.getText().toString();
        String lastNameString = lastName.getText().toString();
        String ageString = age.getText().toString();
        String genderString = gender.getText().toString();
        String telephoneString = telephone.getText().toString();
        String instituteString = institute.getText().toString();
        String departmentString = department.getText().toString();
        String cityString = city.getText().toString();
        String stateString = state.getText().toString();
        String websiteLinksStringIntermediate = websiteLinks.getText().toString();
        //Array Handling
        {
            //JSONArray areasArray = new JSONArray()
            JSONArray websiteLinksArray = new JSONArray();
            //StringTokenizer areasTokenizer = new StringTokenizer(areasStringIntermediate, ",");
            //while (areasTokenizer.hasMoreTokens())
            //    areasArray.put(areasTokenizer.nextToken());
            StringTokenizer websiteLinksTokenizer = new StringTokenizer(websiteLinksStringIntermediate, ",");
            while (websiteLinksTokenizer.hasMoreTokens())
                websiteLinksArray.put(websiteLinksTokenizer.nextToken());
            //areasString = areasArray.toString();
            websiteLinksString = websiteLinksArray.toString();
        }
        {
            if (firstNameString.equals(""))
                makeServerRequest = false;
            if (lastNameString.equals(""))
                makeServerRequest = false;
            if (ageString.equals(""))
                makeServerRequest = false;
            if (telephoneString.equals(""))
                makeServerRequest = false;
            if (instituteString.equals(""))
                makeServerRequest = false;
            if (cityString.equals(""))
                makeServerRequest = false;
            if (stateString.equals(""))
                makeServerRequest = false;
            //if (countryString.equals(""))
            //  makeServerRequest = false;
            //TODO : more checks and display appropriate error messages.
        }
        if (makeServerRequest) {
            JSONObject json = new JSONObject();
            try {
                json.put("username", User.getInstance(getActivity().getApplicationContext()).getUsername());
                json.put("firstName", firstNameString);
                json.put("lastName", lastNameString);
                json.put("age", Integer.parseInt(ageString));
                json.put("gender", genderString);
                json.put("mobileNo", telephoneString);
                json.put("institute", instituteString);
                json.put("department", departmentString);
                json.put("city", cityString);
                json.put("state", stateString);
                //json.put("country", countryString);
                //json.put("areas", areasString);
                json.put("websiteLinks", websiteLinksString);
            } catch (Exception e) {
                Log.d("ERROR", "PROFESSOR BASIC INFO GsonHelper CREATION FAILURE");
            }
            String editUrl = getString(R.string.URL_PROFESSOR_BASIC_INFO) +
                    User.getInstance(getActivity().getApplicationContext()).getUsername() + "/";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.PUT, editUrl, json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //#SensitiveLog
                                Log.d("OUTPUT", response.toString());
                                ProfessorBasicInfo professorBasicInfo =
                                        GsonHelper.getInstance(getActivity().getApplicationContext())
                                                .getGson().fromJson(response.toString(),
                                                ProfessorBasicInfo.class);
                                User.getInstance(getActivity().getApplicationContext())
                                        .getProfessor().basicInfo = professorBasicInfo;

                            } catch (Exception e) {
                                Log.d("ERROR", "PROFESSOR BASIC INFO RESPONSE FAILURE"
                                        + e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("ERROR", "PROFESSOR BASIC INFO VOLLEY FAILURE "
                                    + error.getMessage());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Token "
                            + User.getInstance(getActivity().getApplicationContext()).getToken());
                    return params;
                }

            };
            VolleyHelper.getInstance(getActivity().getApplicationContext())
                    .addToRequestQueue(jsonObjectRequest);
        }
    }

    public void initialize() {
        username.setText(User.getInstance(getActivity()).getUsername());
        firstName.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getFirstName());
        lastName.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getLastName());
        age.setText(String.valueOf(User.getInstance(getActivity()).getProfessor().basicInfo.getAge()));
        telephone.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getMobileNo());
        institute.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getInstitute());
        city.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getCity());
        state.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getState());
        gender.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getGender());
        department.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getDepartment());
        if (User.getInstance(getActivity()).getProfessor().basicInfo.getWebsiteLinks() != null)
            websiteLinks.setText(User.getInstance(getActivity()).getProfessor().basicInfo.getWebsiteLinks()
                    .replace("[", "")
                    .replace("]", "")
                    .replace("\"", ""));
    }
}
