package nitk.ieee.professormatching;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatPreferenceActivity {
    private static final String TAG = "SettingsActivity";//.class.getSimpleName();

    private int easterEgg = 10;
    private Toast toast;
    private AlertDialog alertDialog;

    public static void sendFeedback(Context context) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Send Feedback");
        alertDialog.setMessage("short and sweet!");

        final LayoutInflater inflater = LayoutInflater.from(context);
        final View layout_pwd = inflater.inflate(R.layout.send_feedback, null);
        final MaterialEditText title = (MaterialEditText) layout_pwd.findViewById(R.id.title);
        final MaterialEditText description = (MaterialEditText) layout_pwd.findViewById(R.id.description);

        alertDialog.setView(layout_pwd);
        alertDialog.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String body = "";
                body += title.getText().toString() + " :\n" + description.getText().toString();

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.professormatching_email)});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from Professor Matching");
                intent.putExtra(Intent.EXTRA_TEXT, body);
                inflater.getContext().startActivity(Intent.createChooser(intent, "Choose the app to send"));
            }
        });

        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void changePasswordDialogBox(Context context) {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View layout_pwd = inflater.inflate(R.layout.change_password, null);

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setPositiveButton("CHANGE", null)
                .setTitle("Change Password")
                .setMessage("Fill all boxes")
                .setView(layout_pwd)
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();

        final TextInputEditText editPassword = layout_pwd.findViewById(R.id.editPassword);
        final TextInputEditText editNewPassword = layout_pwd.findViewById(R.id.editNewPassword);
        final TextInputEditText editRepeatPassword = layout_pwd.findViewById(R.id.editRepeatPassword);

        TextInputLayout editPasswordTil = layout_pwd.findViewById(R.id.editPasswordTil);
        TextInputLayout editNewPasswordTil = layout_pwd.findViewById(R.id.editNewPasswordTil);
        TextInputLayout editRepeatPasswordTil = layout_pwd.findViewById(R.id.editRepeatPasswordTil);

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editPasswordTil.setError(null);
                        editNewPasswordTil.setError(null);
                        editRepeatPasswordTil.setError(null);
                        boolean makeRequest = true;
                        String url = context.getString(R.string.URL_CHANGE_PASSWORD);
                        JSONObject json = new JSONObject();
                        try {
                            json.put("username", User.getInstance(context).getUsername());
                            json.put("oldPassword", editPassword.getText().toString());
                            json.put("newPassword", editNewPassword.getText().toString());
                            json.put("confirmPassword", editRepeatPassword.getText().toString());
                        } catch (Exception e) {
                            Log.d(TAG, e.getMessage());
                        }
                        if (!editNewPassword.getText().toString()
                                .equals(editRepeatPassword.getText().toString())) {
                            makeRequest = false;
                            editRepeatPasswordTil.setError("Passwords do not match");
                        } else if (!isPasswordValid(editNewPassword.getText().toString(),
                                editNewPasswordTil)) {
                            makeRequest = false;
                        }
                        if (makeRequest) {
                            JsonObjectRequest passwordChangeRequest = new JsonObjectRequest(Request.Method.POST,
                                    url,
                                    json,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Toast.makeText(context, "Password Changed", Toast.LENGTH_SHORT)
                                                    .show();
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            if (error.getMessage() == null) {
                                                editPasswordTil.setError("Password incorrect");
                                            } else if (error.getMessage()
                                                    .contains("End of input at character 0")) {
                                                Toast.makeText(context, "Password Changed", Toast.LENGTH_SHORT)
                                                        .show();
                                                dialog.dismiss();
                                            }
                                        }
                                    });
                            VolleyHelper.getInstance(context).addToRequestQueue(passwordChangeRequest);
                        }
                    }

                    private boolean isPasswordValid(String password, TextInputLayout textInputEditText) {

                        // At least one uppercase character
                        boolean uppercase = password.matches(".*?[A-Z]+.*");
                        // At least one lowercase character
                        boolean lowercase = password.matches(".*?[a-z]+.*");
                        // At least one special character
                        boolean digit = password.matches(".*?[0-9]+.*");
                        // At least one digit
                        boolean special = password.matches(".*?[#?!@$%^&*-]+.*");
                        // Minimum password length = 8
                        boolean length = password.length() >= 8;
                        String error = "Password should contain : ";
                        if (!uppercase)
                            error += context.getString(R.string.password_uppercase);
                        else if (!lowercase)
                            error += context.getString(R.string.password_lowercase);
                        else if (!digit)
                            error += context.getString(R.string.password_digit);
                        else if (!special)
                            error += context.getString(R.string.password_special);
                        else if (!length)
                            error += context.getString(R.string.password_length);
                        if (uppercase && lowercase && special && digit && length)
                            textInputEditText.setError(null);
                        else
                            textInputEditText.setError(error);
                        return uppercase && lowercase && special && digit && length;
                    }
                });
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        //ActionBar actionBar =getSupportActionBar();
        Toolbar toolbar = findViewById(R.id.settings_toolbar);
        toolbar.setTitle("Settings");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        addPreferencesFromResource(R.xml.pref_main);


        // feedback preference click listener
        Preference myPref = findPreference("key_send_feedback");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                sendFeedback(SettingsActivity.this);
                return true;
            }
        });
        final Preference changePassword = findPreference("change_password");
        changePassword.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                changePasswordDialogBox(SettingsActivity.this);
                return true;
            }
        });

        Preference delete = findPreference("delete_account");
        delete.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this,
                        R.style.Theme_AppCompat_Dialog_Alert);
                builder.setMessage("Are you sure?")
                        .setCancelable(true)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String url = "";
                                if (User.getInstance(getApplicationContext()).getType().equals("prof"))
                                    url = getString(R.string.URL_DELETE_PROFESSOR)
                                            + User.getInstance(getApplicationContext()).getUsername()
                                            + "/";
                                else
                                    url = getString(R.string.URL_DELETE_STUDENT)
                                            + User.getInstance(getApplicationContext()).getUsername()
                                            + "/";
                                StringRequest deleteUserRequest = new StringRequest(Request.Method.DELETE,
                                        url,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                Log.d(TAG, "Successfully deleted user");
                                                User.getInstance(getApplicationContext()).logout();
                                                Intent i = getBaseContext().getPackageManager().
                                                        getLaunchIntentForPackage(getBaseContext().getPackageName());
                                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(i);
                                                finish();
                                                System.exit(0);
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Log.d(TAG, "Error while deleting user" + error.getMessage());
                                            }
                                        }) {
                                    @Override
                                    public Map<String, String> getHeaders() {
                                        Map<String, String> headers = new HashMap<>();
                                        headers.put("Authorization",
                                                "Token " + User.getInstance(getApplicationContext()).getToken());
                                        return headers;
                                    }
                                };
                                VolleyHelper.getInstance(getApplicationContext())
                                        .addToRequestQueue(deleteUserRequest);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                alertDialog = builder.create();
                alertDialog.setTitle("Delete Account");
                alertDialog.show();
                return true;
            }
        });
        Preference version = findPreference("version");
        toast = Toast.makeText(getApplicationContext(),
                easterEgg + " steps away from a chicken",
                Toast.LENGTH_SHORT);
        version.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (easterEgg-- < 6 && easterEgg > 0) {
                    toast.setText(easterEgg + " steps away from a chicken");
                    toast.show();
                } else if (easterEgg == 0) {
                    toast.setText("10 steps away from a new chicken");
                    toast.show();
                    easterEgg = 10;
                }
                return true;
            }
        });
    }
}
