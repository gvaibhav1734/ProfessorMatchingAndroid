package nitk.ieee.professormatching;

import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * Sets up a simple UI for registering a user
 */
public class RegisterActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    private static final String TAG = "RegisterActivity";
    // UI references.
    private AutoCompleteTextView mEmailView;
    private TextInputEditText mPasswordView;
    private TextInputEditText mUsernameView;
    private TextInputLayout mPasswordViewTil;
    private TextInputLayout mUsernameViewTil;
    private TextInputLayout mEmailViewTil;
    private View rootView;
    private RadioGroup mRadioGroup;
    private ProgressBar mProgressBar;
    private boolean clicked;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("username", mUsernameView.getText().toString());
        outState.putString("email", mEmailView.getText().toString());
        outState.putString("password", mPasswordView.getText().toString());
        outState.putInt("type", mRadioGroup.getCheckedRadioButtonId());
    }

    private Button mRegisterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //Set up toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rootView = findViewById(R.id.register_cl_root);
        mProgressBar = findViewById(R.id.register_progress);
        // Set up the register form.
        mEmailView = findViewById(R.id.register_actv_email);
        populateAutoComplete();
        mPasswordViewTil = findViewById(R.id.register_til_password);
        mEmailViewTil = findViewById(R.id.register_til_email);
        mUsernameViewTil = findViewById(R.id.register_til_username);
        mPasswordView = findViewById(R.id.register_tiet_password);
        mUsernameView = findViewById(R.id.register_tiet_username);
        mRadioGroup = findViewById(R.id.register_rg);
        if (savedInstanceState != null) {
            mUsernameView.setText(savedInstanceState.getString("username"));
            mEmailView.setText(savedInstanceState.getString("email"));
            mPasswordView.setText(savedInstanceState.getString("password"));
            mRadioGroup.check(savedInstanceState.getInt("type"));
        }
        mRegisterButton = findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!clicked) {
                    toggleRegister();
                    attemptRegistration(mUsernameView.getText().toString(), mEmailView.getText().toString(),
                            mPasswordView.getText().toString());
                }
            }
        });
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), RegisterActivity.ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(RegisterActivity.ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(RegisterActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    private boolean isPasswordValid(String password) {

        // At least one uppercase character
        boolean uppercase = password.matches(".*?[A-Z]+.*");
        // At least one lowercase character
        boolean lowercase = password.matches(".*?[a-z]+.*");
        // At least one special character
        boolean digit = password.matches(".*?[0-9]+.*");
        // At least one digit
        boolean special = password.matches(".*?[#?!@$%^&*-]+.*");
        // Minimum password length = 8
        boolean length = password.length() >= 8;
        String error = "Password should contain : ";
        if (!uppercase)
            error += getString(R.string.password_uppercase);
        else if (!lowercase)
            error += getString(R.string.password_lowercase);
        else if (!digit)
            error += getString(R.string.password_digit);
        else if (!special)
            error += getString(R.string.password_special);
        else if (!length)
            error += getString(R.string.password_length);
        if (uppercase && lowercase && special && digit && length)
            mPasswordViewTil.setError(null);
        else
            mPasswordViewTil.setError(error);
        return uppercase && lowercase && special && digit && length;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace regex with a better one after looking up RFC882 whatever the hell that is!
        boolean check = email.matches(".*?@gmail.com$");
        if (!check)
            mEmailViewTil.setError("Wrong email");
        return check;
    }

    private void attemptRegistration(String username, String email, String password) {
        //Reset Errors
        mUsernameViewTil.setError(null);
        mEmailViewTil.setError(null);
        mPasswordViewTil.setError(null);
        boolean cancel = false;

        if (TextUtils.isEmpty(username)) {
            cancel = true;
            mUsernameViewTil.setError(getString(R.string.error_field_required));
        } else if (TextUtils.isEmpty(email)) {
            cancel = true;
            mEmailViewTil.setError(getString(R.string.error_field_required));
        } else if (!isEmailValid(email)) {
            cancel = true;
            mEmailViewTil.setError("Wrong email");
        } else if (!isPasswordValid(password)) {
            cancel = true;
        } else if (mRadioGroup.getCheckedRadioButtonId() == -1)//None of them is checked.
        {
            cancel = true;
            Snackbar.make(rootView
                    , "Tick any one"
                    , Snackbar.LENGTH_LONG).show();
        }

        if (cancel) {
            // There was an error cancel registration.
            toggleRegister();
        } else {
            String registerUserUrl;
            if (mRadioGroup.getCheckedRadioButtonId() == R.id.register_rb_student)
                registerUserUrl = getString(R.string.URL_REGISTER_STUDENT);
            else
                registerUserUrl = getString(R.string.URL_REGISTER_PROF);
            if (isEmailValid(email) && isPasswordValid(password)) {
                JSONObject json = new JSONObject();
                try {
                    json.put("username", username);
                    json.put("email", email);
                    json.put("password", password);
                } catch (Exception e) {
                    Log.d(TAG, "PROFESSOR BASIC INFO GsonHelper CREATION FAILURE");
                }
                JsonObjectRequest stringRequest = new JsonObjectRequest(
                        Request.Method.POST, registerUserUrl, json,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    String username = String.valueOf(response.get("username"));
                                    String email = String.valueOf(response.get("email"));
                                    String token = String.valueOf(response.get("token"));
                                    String type = String.valueOf(response.get("type"));
                                    User.getInstance(getApplicationContext()).setData(username, email, token, type);
                                    mProgressBar.setVisibility(View.GONE);
                                    Intent a;
                                    if (User.getInstance(getApplicationContext()).getType().equals("prof")) {
                                        a = new Intent(getApplicationContext(), ProfessorBasicInfoEntryActivity.class);
                                    } else
                                        a = new Intent(getApplicationContext(), StudentInfoEntryActivity.class);
                                    startActivity(a);
                                } catch (org.json.JSONException e) {
                                    Log.d(TAG, "REGISTER USER RESPONSE FAILURE");
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("ERROR", "USER NOT REGISTERED " + error.networkResponse.headers);
                                try {
                                    Log.d("ERROR", "USER NOT REGISTERED "
                                            + new String(error.networkResponse.data, "UTF-8"));
                                    JSONObject errorJson = new JSONObject(
                                            new String(error.networkResponse.data, "UTF-8"));
                                    if (!errorJson.isNull("email")) {
                                        if (errorJson.get("email")
                                                .toString()
                                                .contains("This field must be unique.")) {
                                            mEmailViewTil.setError("Email id already used");
                                            toggleRegister();
                                        }
                                    } else if (!errorJson.isNull("username")) {
                                        if (errorJson.get("username")
                                                .toString()
                                                .contains("This field must be unique.")) {
                                            mUsernameViewTil.setError("Username already in use");
                                            toggleRegister();
                                        }
                                    }
                                } catch (Exception e) {
                                    Log.d(TAG, "Network Response Json error");
                                }
                            }
                        }) {
                };
                VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
            }
        }
    }

    private void toggleRegister() {
        if (clicked) {
            mProgressBar.setVisibility(View.GONE);
            mRegisterButton.setVisibility(View.VISIBLE);
            clicked = false;
        } else {
            clicked = true;
            mProgressBar.setVisibility(View.VISIBLE);
            mRegisterButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}