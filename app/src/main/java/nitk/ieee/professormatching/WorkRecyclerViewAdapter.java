package nitk.ieee.professormatching;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class WorkRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "WorkListAdapter";
    private Context context;
    private FragmentManager fragmentManager;

    private List<WorkEx> workEx = new ArrayList<>();
    private List<PersonalProjects> personalProjects = new ArrayList<>();
    private boolean editable ;

    public WorkRecyclerViewAdapter(Context context, FragmentManager fragmentManager, List<WorkEx> workEx
            , List<PersonalProjects> personalProjects) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.editable = editable;
        if (workEx != null)
            this.workEx = workEx;
        if (personalProjects != null)
            this.personalProjects = personalProjects;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public List<WorkEx> getWorkExList() {
        return this.workEx;
    }

    public List<PersonalProjects> getPersonalProjectsList() {
        return this.personalProjects;
    }

    public void insertItem(WorkEx workExItem) {
        this.workEx.add(workExItem);
        Log.d(TAG, "add WorkEx " + workExItem.getOrganization());
        notifyItemInserted(this.workEx.size() - 1);
    }

    public void insertItem(PersonalProjects personalProjects) {
        this.personalProjects.add(personalProjects);
        Log.d(TAG, "add personal " + personalProjects.getTitle());
        notifyItemInserted(this.personalProjects.size() + this.workEx.size() - 1);
    }

    public void changeItemAt(int position, WorkEx modified) {
        WorkEx original = workEx.get(position);
        Log.d(TAG, "change WorkEx " + original.getOrganization());
        original.setOrganization(modified.getOrganization());
        original.setDuration(modified.getDuration());
        original.setDescription(modified.getDescription());
        Log.d(TAG, "changed WorkEx " + original.getOrganization());
        notifyItemChanged(position);
    }

    public void changeItemAt(int itemPosition, PersonalProjects modified) {
        PersonalProjects original = this.personalProjects.get(itemPosition - this.workEx.size());
        Log.d(TAG, "change Personal Projects " + original.getTitle());
        original.setTitle(modified.getTitle());
        original.setDescription(modified.getDescription());
        Log.d(TAG, "changed Personal Projects " + original.getTitle());
        notifyItemChanged(itemPosition);
    }

    public void deleteWorkExAt(int itemPosition) {
        Log.d(TAG, "delete WorkEx " + itemPosition);
        this.workEx.remove(itemPosition);
        notifyItemRemoved(itemPosition);
    }

    public void deletePersonalProjectsAt(int itemPosition) {
        Log.d(TAG, "delete Personal Projects " + itemPosition);
        this.personalProjects.remove(itemPosition - this.workEx.size());
        notifyItemRemoved(itemPosition);
    }

    class ViewHolderWorkEx extends RecyclerView.ViewHolder {
        TextView organization, duration, description;

        public ViewHolderWorkEx(View itemView) {
            super(itemView);
            organization = itemView.findViewById(R.id.user_profile_work_tv_organization);
            duration = itemView.findViewById(R.id.user_profile_work_tv_duration);
            description = itemView.findViewById(R.id.user_profile_work_tv_description);
        }
    }

    class ViewHolderPersonalProjects extends RecyclerView.ViewHolder {
        TextView title, description;

        public ViewHolderPersonalProjects(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.user_profile_work_tv_title);
            description = itemView.findViewById(R.id.user_profile_work_tv_description);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getType " + position);
        if (position < workEx.size())
            return 1;
        else
            return 2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "create " + viewType);
        switch (viewType) {
            case 1:
                View v1 = LayoutInflater.from(context)
                        .inflate(R.layout.user_profile_work_work_ex_item, parent, false);
                return new ViewHolderWorkEx(v1);
            case 2:
                View v2 = LayoutInflater.from(context)
                        .inflate(R.layout.user_profile_work_personal_projects_item, parent, false);
                return new ViewHolderPersonalProjects(v2);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "bind " + holder.getItemViewType() + " " + position);
        switch (holder.getItemViewType()) {
            case 1:
                ViewHolderWorkEx localHolder1 = (ViewHolderWorkEx) holder;
                localHolder1.organization.setText(workEx.get(holder.getAdapterPosition()).getOrganization());
                localHolder1.duration.setText(String.valueOf(workEx.get(holder.getAdapterPosition()).getDuration()));
                localHolder1.description.setText(workEx.get(holder.getAdapterPosition()).getDescription());
                localHolder1.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WorkDialogFragment workDialogFragment =
                                WorkDialogFragment.newInstance(workEx.get(holder.getAdapterPosition())
                                        , editable
                                        , holder.getAdapterPosition());
                        workDialogFragment.setAdapter(WorkRecyclerViewAdapter.this);
                        workDialogFragment.show(fragmentManager, "dialog");
                    }
                });

                break;
            case 2:
                int localPosition = holder.getAdapterPosition() - workEx.size();
                ViewHolderPersonalProjects localHolder2 = (ViewHolderPersonalProjects) holder;
                localHolder2.title.setText(personalProjects.get(localPosition).getTitle());
                localHolder2.description.setText(personalProjects.get(localPosition).getDescription());
                localHolder2.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WorkDialogFragment workDialogFragment =
                                WorkDialogFragment.newInstance(personalProjects.get(localPosition)
                                        , editable
                                        , holder.getAdapterPosition());
                        workDialogFragment.setAdapter(WorkRecyclerViewAdapter.this);
                        workDialogFragment.show(fragmentManager, "dialog");
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return workEx.size() + personalProjects.size();
    }
}