package nitk.ieee.professormatching;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class ProfessorBasicInfoEntryActivity extends AppCompatActivity {

    private TextInputEditText firstName, lastName, age, telephone, institute, city, state, country, areas, websiteLinks, department;
    private Button update;
    private Spinner gender;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("firstName", firstName.getText().toString());
        outState.putString("lastName", lastName.getText().toString());
        outState.putString("age", age.getText().toString());
        outState.putString("telephone", telephone.getText().toString());
        outState.putString("institute", institute.getText().toString());
        outState.putString("department", department.getText().toString());
        outState.putString("city", city.getText().toString());
        outState.putString("state", state.getText().toString());
        outState.putString("country", country.getText().toString());
        outState.putString("areas", areas.getText().toString());
        outState.putString("websiteLinks", websiteLinks.getText().toString());
        outState.putInt("gender", gender.getSelectedItemPosition());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professor_basic_info_entry);

        Toolbar toolbar = findViewById(R.id.professor_basic_info_tb);
        toolbar.setTitle("Professor Basic Info");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ArrayAdapter<CharSequence> genderAdapter = ArrayAdapter
                .createFromResource(this, R.array.gender_options,
                        android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        update = findViewById(R.id.professor_basic_info_btn_update);
        firstName = findViewById(R.id.professor_basic_info_tiet_firstName);
        lastName = findViewById(R.id.professor_basic_info_tiet_lastName);
        age = findViewById(R.id.professor_basic_info_tiet_age);
        telephone = findViewById(R.id.professor_basic_info_tiet_telephone);
        institute = findViewById(R.id.professor_basic_info_tiet_institute);
        department = findViewById(R.id.professor_basic_info_tiet_department);
        city = findViewById(R.id.professor_basic_info_tiet_city);
        state = findViewById(R.id.professor_basic_info_tiet_state);
        country = findViewById(R.id.professor_basic_info_tiet_country);
        areas = findViewById(R.id.professor_basic_info_tiet_areas);
        websiteLinks = findViewById(R.id.professor_basic_info_tiet_website_links);
        gender = findViewById(R.id.professor_basic_info_sp_gender);
        gender.setAdapter(genderAdapter);
        if (savedInstanceState != null) {
            firstName.setText(savedInstanceState.getString("firstName"));
            lastName.setText(savedInstanceState.getString("lastName"));
            age.setText(savedInstanceState.getString("age"));
            telephone.setText(savedInstanceState.getString("telephone"));
            institute.setText(savedInstanceState.getString("institute"));
            department.setText(savedInstanceState.getString("department"));
            city.setText(savedInstanceState.getString("city"));
            state.setText(savedInstanceState.getString("state"));
            country.setText(savedInstanceState.getString("country"));
            areas.setText(savedInstanceState.getString("areas"));
            websiteLinks.setText(savedInstanceState.getString("websiteLinks"));
            gender.setSelection(savedInstanceState.getInt("gender"));
        } else {
            gender.setSelection(0);
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean makeServerRequest = true;
                String areasString, websiteLinksString;
                String firstNameString = firstName.getText().toString();
                String lastNameString = lastName.getText().toString();
                String ageString = age.getText().toString();
                String genderString = gender.getSelectedItem().toString();
                String telephoneString = telephone.getText().toString();
                String instituteString = institute.getText().toString();
                String departmentString = department.getText().toString();
                String cityString = city.getText().toString();
                String stateString = state.getText().toString();
                String countryString = country.getText().toString();
                String areasStringIntermediate = areas.getText().toString();
                String websiteLinksStringIntermediate = websiteLinks.getText().toString();
                //Array Handling
                {
                    JSONArray areasArray = new JSONArray(), websiteLinksArray = new JSONArray();
                    StringTokenizer areasTokenizer = new StringTokenizer(areasStringIntermediate, ",");
                    while (areasTokenizer.hasMoreTokens())
                        areasArray.put(areasTokenizer.nextToken());
                    StringTokenizer websiteLinksTokenizer = new StringTokenizer(websiteLinksStringIntermediate, ",");
                    while (websiteLinksTokenizer.hasMoreTokens())
                        websiteLinksArray.put(websiteLinksTokenizer.nextToken());
                    areasString = areasArray.toString();
                    websiteLinksString = websiteLinksArray.toString();
                }
                {
                    if (firstNameString.equals(""))
                        makeServerRequest = false;
                    if (lastNameString.equals(""))
                        makeServerRequest = false;
                    if (ageString.equals(""))
                        makeServerRequest = false;
                    if (telephoneString.equals(""))
                        makeServerRequest = false;
                    if (instituteString.equals(""))
                        makeServerRequest = false;
                    if (cityString.equals(""))
                        makeServerRequest = false;
                    if (stateString.equals(""))
                        makeServerRequest = false;
                    if (countryString.equals(""))
                        makeServerRequest = false;
                    //TODO : more checks and display appropriate error messages.
                }
                if (makeServerRequest) {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("username", User.getInstance(getApplicationContext()).getUsername());
                        json.put("firstName", firstNameString);
                        json.put("lastName", lastNameString);
                        json.put("age", Integer.parseInt(ageString));
                        json.put("gender", genderString);
                        json.put("mobileNo", telephoneString);
                        json.put("institute", instituteString);
                        json.put("department", departmentString);
                        json.put("city", cityString);
                        json.put("state", stateString);
                        json.put("country", countryString);
                        json.put("areas", areasString);
                        json.put("websiteLinks", websiteLinksString);
                    } catch (Exception e) {
                        Log.d("ERROR", "PROFESSOR BASIC INFO GsonHelper CREATION FAILURE");
                    }
                    String editUrl = getString(R.string.URL_PROFESSOR_BASIC_INFO) +
                            User.getInstance(getApplicationContext()).getUsername() + "/";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            Request.Method.PUT, editUrl, json,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        //#SensitiveLog
                                        Log.d("OUTPUT", response.toString());
                                        ProfessorBasicInfo professorBasicInfo =
                                                GsonHelper.getInstance(getApplicationContext())
                                                        .getGson().fromJson(response.toString(),
                                                        ProfessorBasicInfo.class);
                                        User.getInstance(getApplicationContext())
                                                .getProfessor().basicInfo = professorBasicInfo;
                                        Intent a = new Intent(getApplicationContext(),
                                                ProfessorDetailInfoEntryActivity.class);
                                        startActivity(a);
                                    } catch (Exception e) {
                                        Log.d("ERROR", "PROFESSOR BASIC INFO RESPONSE FAILURE"
                                                + e.getMessage());
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("ERROR", "PROFESSOR BASIC INFO VOLLEY FAILURE "
                                            + error.getMessage());
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Authorization", "Token "
                                    + User.getInstance(getApplicationContext()).getToken());
                            return params;
                        }

                    };
                    VolleyHelper.getInstance(getApplicationContext())
                            .addToRequestQueue(jsonObjectRequest);
                } else {
                    Snackbar.make(view.getRootView(), "All fields are mandatory"
                            , Snackbar.LENGTH_SHORT);
                }
            }
        });

    }
}
