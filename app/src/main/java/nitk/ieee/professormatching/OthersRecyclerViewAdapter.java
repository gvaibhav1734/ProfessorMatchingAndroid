package nitk.ieee.professormatching;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class OthersRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "OthersListAdapter";
    public static final int AREAS = 0;
    public static final int KEYWORDS = 1;
    public static final int HOBBIES = 2;
    private Context context;
    private FragmentManager fragmentManager;
    private SkillsInterest skillsInterest;

    private List<String> hobbies = new ArrayList<>();
    private List<String> areas = new ArrayList<>();
    private List<String> keywords = new ArrayList<>();
    private boolean editable;

    public OthersRecyclerViewAdapter(Context context
            , FragmentManager fragmentManager
            , SkillsInterest skillsInterests
            , List<String> hobbies) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.editable = editable;
        if (skillsInterests != null) {
            this.skillsInterest = skillsInterests;
            this.keywords = skillsInterests.getKeywords();
            this.areas = skillsInterests.getAreas();
        }
        if (hobbies != null)
            this.hobbies = hobbies;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public SkillsInterest getSkillsInterest() {
        SkillsInterest skillsInterest = new SkillsInterest();
        skillsInterest.setAreas(areas);
        skillsInterest.setKeywords(keywords);
        return skillsInterest;
    }

    public List<String> getAreasList() {
        return this.areas;
    }

    public List<String> getKeywordsList() {
        return this.keywords;
    }

    public List<String> getHobbiesList() {
        return this.hobbies;
    }

    public void insertItem(String item, int type) {
        if (type == AREAS) {
            this.areas.add(item);
            Log.d(TAG, "add Areas " + item);
        } else if (type == KEYWORDS) {
            this.keywords.add(item);
            Log.d(TAG, "add Keywords " + item);
        } else if (type == HOBBIES) {
            this.hobbies.add(item);
            Log.d(TAG, "add Hobbies " + item);
        }
        notifyItemInserted(this.areas.size() + this.keywords.size() + this.hobbies.size() - 1);
    }

    public void changeItemAt(int itemPosition, String modified, int type) {
        if (type == AREAS) {
            int localPosition = itemPosition;
            String original = this.areas.get(localPosition);
            Log.d(TAG, "change areas " + original);
            this.areas.remove(localPosition);
            this.areas.add(localPosition, modified);
            Log.d(TAG, "changed areas " + this.areas.get(localPosition));
        } else if (type == KEYWORDS) {
            int localPosition = itemPosition - this.areas.size();
            String original = this.keywords.get(localPosition);
            Log.d(TAG, "change keywords " + original);
            this.keywords.remove(localPosition);
            this.keywords.add(localPosition, modified);
            Log.d(TAG, "changed keywords " + this.keywords.get(localPosition));
        } else if (type == HOBBIES) {
            int localPosition = itemPosition - this.areas.size() - this.keywords.size();
            String original = this.hobbies.get(localPosition);
            Log.d(TAG, "change hobbies " + original);
            this.hobbies.remove(localPosition);
            this.hobbies.add(localPosition, modified);
            Log.d(TAG, "changed hobbies " + this.hobbies.get(localPosition));
        }
        notifyItemChanged(itemPosition);
    }

    public void deleteAreasAt(int itemPosition) {
        Log.d(TAG, "delete areas" + itemPosition);
        this.areas.remove(itemPosition);
        notifyItemRemoved(itemPosition);
    }

    public void deleteKeywordsAt(int itemPosition) {
        Log.d(TAG, "delete keywords" + itemPosition);
        this.keywords.remove(itemPosition - this.areas.size());
        notifyItemRemoved(itemPosition);
    }

    public void deleteHobbiesAt(int itemPosition) {
        Log.d(TAG, "delete hobbies" + itemPosition);
        this.hobbies.remove(itemPosition - this.areas.size() - this.keywords.size());
        notifyItemRemoved(itemPosition);
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getType " + position);
        if (position < areas.size())
            return AREAS;
        else if (position < areas.size() + keywords.size())
            return KEYWORDS;
        else
            return HOBBIES;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "create " + viewType);
        switch (viewType) {
            case AREAS:
                View v1 = LayoutInflater.from(context)
                        .inflate(R.layout.user_profile_others_areas_item, parent, false);
                return new ViewHolderAreas(v1);
            case KEYWORDS:
                View v2 = LayoutInflater.from(context)
                        .inflate(R.layout.user_profile_others_keywords_item, parent, false);
                return new ViewHolderKeywords(v2);
            case HOBBIES:
                View v3 = LayoutInflater.from(context)
                        .inflate(R.layout.user_profile_others_hobbies_item, parent, false);
                return new ViewHolderHobbies(v3);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "bind " + holder.getItemViewType() + " " + position);
        switch (holder.getItemViewType()) {
            case AREAS:
                ViewHolderAreas localHolder1 = (ViewHolderAreas) holder;
                localHolder1.areas.setText(areas.get(holder.getAdapterPosition()));
                localHolder1.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OthersDialogFragment othersDialogFragment =
                                OthersDialogFragment.newInstance(areas.get(localHolder1.getAdapterPosition())
                                        , AREAS
                                        , editable
                                        , holder.getAdapterPosition());
                        othersDialogFragment.setAdapter(OthersRecyclerViewAdapter.this);
                        othersDialogFragment.show(fragmentManager, "dialog");
                    }
                });
                break;

            case KEYWORDS:
                int localPosition = holder.getAdapterPosition() - areas.size();
                ViewHolderKeywords localHolder2 = (ViewHolderKeywords) holder;
                localHolder2.keywords.setText(keywords.get(localPosition));
                localHolder2.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OthersDialogFragment othersDialogFragment =
                                OthersDialogFragment.newInstance(keywords.get(localPosition)
                                        , KEYWORDS
                                        , editable
                                        , holder.getAdapterPosition());
                        othersDialogFragment.setAdapter(OthersRecyclerViewAdapter.this);
                        othersDialogFragment.show(fragmentManager, "dialog");
                    }
                });
                break;

            case HOBBIES:
                int localPosition1 = holder.getAdapterPosition() - areas.size() - keywords.size();
                ViewHolderHobbies localHolder3 = (ViewHolderHobbies) holder;
                localHolder3.hobby.setText(hobbies.get(localPosition1));
                localHolder3.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OthersDialogFragment othersDialogFragment =
                                OthersDialogFragment.newInstance(hobbies.get(localPosition1)
                                        , HOBBIES
                                        , editable
                                        , holder.getAdapterPosition());
                        othersDialogFragment.setAdapter(OthersRecyclerViewAdapter.this);
                        othersDialogFragment.show(fragmentManager, "dialog");
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return areas.size() + hobbies.size() + keywords.size();
    }

    class ViewHolderHobbies extends RecyclerView.ViewHolder {
        TextView hobby;

        public ViewHolderHobbies(View itemView) {
            super(itemView);
            hobby = itemView.findViewById(R.id.user_profile_others_hobbies_tv_hobby);
        }
    }

    class ViewHolderAreas extends RecyclerView.ViewHolder {
        TextView areas;

        public ViewHolderAreas(View itemView) {
            super(itemView);
            areas = itemView.findViewById(R.id.user_profile_others_skills_tv_area);
        }
    }

    class ViewHolderKeywords extends RecyclerView.ViewHolder {
        TextView keywords;

        public ViewHolderKeywords(View itemView) {
            super(itemView);
            keywords = itemView.findViewById(R.id.user_profile_others_skills_tv_keyword);
        }
    }
}