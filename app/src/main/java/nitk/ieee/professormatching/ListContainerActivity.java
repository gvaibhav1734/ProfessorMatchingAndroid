package nitk.ieee.professormatching;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


/**
 * ListContainerActivity is used to display the three lists(Unselected, Selected, All) of students.
 * The professor can select the students.
 * List of selected students is uploaded to the server when the user quits this activity.
 */
public class ListContainerActivity extends AppCompatActivity {
    private final static String TAG = "ListActivity";
    private ViewPager viewPager;
    private ConstraintLayout listParent;
    private ProgressBar progressBar;
    ListRecyclerViewAdapter mAllAdapter;
    ListRecyclerViewAdapter mSelectedAdapter;
    ListRecyclerViewAdapter mUnselectedAdapter;

    @Override
    protected void onStop() {
        super.onStop();
        String url = getString(R.string.URL_PROFESSOR_DETAILED_INFO)
                + User.getInstance(getApplicationContext()).getUsername()
                + "/";
        String selectedStudents = "";
        for (int i = 0; i < mSelectedAdapter.getList().size(); i++) {
            selectedStudents += mSelectedAdapter.getList().get(i).getUsername()
                    + (i == (mSelectedAdapter.getList().size() - 1) ? "" : ",");
        }
        Log.d(TAG, "Custom string " + selectedStudents);
        JSONObject json = null;
        try {
            json = new JSONObject();
            json.put("username", User.getInstance(getApplicationContext()).getUsername());
            json.put("minCgpa"
                    , User.getInstance(getApplicationContext())
                            .getProfessor()
                            .detailInfo.getMinCgpa());
            json.put("minWorkEx"
                    , User.getInstance(getApplicationContext())
                            .getProfessor()
                            .detailInfo.getMinWorkEx());
            json.put("minYearOfStudy"
                    , User.getInstance(getApplicationContext())
                            .getProfessor()
                            .detailInfo.getMinYearOfStudy());
            json.put("selectedStudents", selectedStudents);
        } catch (Exception e) {
            Log.d(TAG, "Error while creating JSON in save instance state." + e.getMessage());
        }
        JsonObjectRequest sendSelectedStudents = new JsonObjectRequest(Request.Method.PUT
                , url
                , json
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Saved selection", Toast.LENGTH_SHORT)
                        .show();
                Log.d(TAG, "save instance state selected students successful");
                Log.d(TAG, response.toString());
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "save instance state selected students failure" + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Token "
                        + User.getInstance(getApplicationContext()).getToken());
                return params;
            }
        };
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(sendSelectedStudents);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("all"
                , (ArrayList<? extends Parcelable>) mAllAdapter.getList());
        outState.putParcelableArrayList("selected"
                , (ArrayList<? extends Parcelable>) mSelectedAdapter.getList());
        outState.putParcelableArrayList("unselected"
                , (ArrayList<? extends Parcelable>) mUnselectedAdapter.getList());
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_container);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("List of students");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        progressBar = findViewById(R.id.progress);
        listParent = findViewById(R.id.list_container_ll_root);
        TabLayout listTabs = findViewById(R.id.list_container_tl);
        viewPager = findViewById(R.id.a_list_container_vp);
        listTabs.setupWithViewPager(viewPager);
        progressBar.setVisibility(View.VISIBLE);

        mAllAdapter = new ListRecyclerViewAdapter(this, ListRecyclerViewAdapter.ALL);
        mSelectedAdapter = new ListRecyclerViewAdapter(this, ListRecyclerViewAdapter.SELECTED);
        mUnselectedAdapter = new ListRecyclerViewAdapter(this, ListRecyclerViewAdapter.UNSELECTED);
        if (savedInstanceState != null) {
            progressBar.setVisibility(View.GONE);
            mAllAdapter.setList(savedInstanceState.getParcelableArrayList("all"));
            mSelectedAdapter.setList(savedInstanceState.getParcelableArrayList("selected"));
            mUnselectedAdapter.setList(savedInstanceState.getParcelableArrayList("unselected"));

        } else {
            //Loading selected
            String url = getString(R.string.URL_PROFESSOR_DETAILED_INFO)
                    + User.getInstance(getApplicationContext()).getUsername()
                    + "/";
            JsonObjectRequest getList = new JsonObjectRequest(Request.Method.GET
                    , url
                    , null
                    , new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    new BackgroundListLoadTask().execute(response);
                }
            }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Selected " + error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Token "
                            + User.getInstance(getApplicationContext()).getToken());
                    return params;
                }

            };
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getList);
        }
        viewPager.setAdapter(new CustomListPagerAdapter());
        viewPager.setCurrentItem(CustomListPagerAdapter.ALL_TAB);
    }

    class BackgroundListLoadTask extends AsyncTask<JSONObject, Void, Void> {
        @Override
        protected Void doInBackground(JSONObject... jsonObjects) {
            try {
                JSONObject response = jsonObjects[0];
                List<Student> selected = new ArrayList<>();
                Log.d(TAG, response.toString());
                if (!response.isNull("selectedStudents")
                        && !response.getString("selectedStudents").equals("")) {
                    String selectedStudents = response.getString("selectedStudents");
                    selectedStudents = selectedStudents.replace("\"[", "");
                    selectedStudents = selectedStudents.replace("]\"", "");
                    StringTokenizer stringTokenizer =
                            new StringTokenizer(selectedStudents, ",");
                    Log.d(TAG, selectedStudents);
                    while (stringTokenizer.hasMoreTokens()) {
                        String username = stringTokenizer.nextToken();
                        Student student = new Student();
                        student.setUsername(username);
                        student.setSelected(true);
                        selected.add(student);
                    }
                }

                String url = getString(R.string.URL_GET_LIST)
                        + User.getInstance(getApplicationContext()).getUsername()
                        + "/";
                JsonArrayRequest getList = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    List<Student> all = new ArrayList<>();
                                    List<Student> unselected = new ArrayList<>();
                                    for (int i = 0; i < response.length(); i++) {
                                        String username = response
                                                .getJSONObject(i)
                                                .getString("username");
                                        Student student = new Student();
                                        student.setUsername(username);
                                        if (selected.contains(student)) {
                                            selected.contains(student);
                                            student.setSelected(true);
                                        } else {
                                            student.setSelected(false);
                                            unselected.add(student);
                                        }
                                        all.add(student);


                                    }
                                    Log.d(TAG, "Selected Students " + selected.toString());
                                    new GetDetailsTask("All", all, mAllAdapter).execute();
                                    new GetDetailsTask("Selected", selected, mSelectedAdapter).execute();
                                    new GetDetailsTask("Unselected", unselected, mUnselectedAdapter).execute();
                                } catch (Exception e) {
                                    Log.d(TAG, e.getMessage());

                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, error.getMessage());
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Authorization", "Token "
                                + User.getInstance(getApplicationContext()).getToken());
                        return params;
                    }

                };
                VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getList);

            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
            return null;
        }
    }

    class GetDetailsTask extends AsyncTask<Void, Void, Void> {
        private ListRecyclerViewAdapter adapter;
        private String listType;
        private List<Student> students;

        GetDetailsTask(String listType, List<Student> students, ListRecyclerViewAdapter adapter) {
            this.listType = listType;
            this.adapter = adapter;
            this.students = students;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (students.size() > 0)
                getDetails(students.get(0));
            return null;
        }

        private void getDetails(Student student) {
            String url = getString(R.string.URL_STUDENT_BASIC_INFO) + student.getUsername() + "/";
            JsonObjectRequest getDetails = new JsonObjectRequest(Request.Method.GET,
                    url,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (students.size() > 1) {
                                    students.remove(0);
                                    getDetails(students.get(0));
                                }
                                student.studentBasicInfo
                                        .setFirstName(response.getString("firstName"));
                                student.studentBasicInfo
                                        .setCity(response.getString("city"));
                                student.studentBasicInfo
                                        .setState(response.getString("state"));
                                adapter.addStudent(student);
                                Log.d(TAG, "Get details " + student.getUsername() + "");
                                if (listType.equals("All"))
                                    progressBar.setVisibility(View.GONE);
                            } catch (Exception e) {
                                Log.d(TAG, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, "Volley Error" + error.getMessage());
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Token "
                            + User.getInstance(getApplicationContext()).getToken());
                    return params;
                }
            };
            VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(getDetails);
        }
    }

    /**
     * Tabview + pager for the custom list.
     * This class serves as a adapter to make the tabview and pager work in sync
     * It makes a VolleyRequest to get the list of students.
     * The students obtained are then updated in the setup method to display the data in recycler view.
     */
    class CustomListPagerAdapter extends PagerAdapter {
        public static final String TAG = "CustomPagerAdapter";

        static final int UNSELECTED_TAB = 0;
        static final int ALL_TAB = 1;
        static final int SELECTED_TAB = 2;

        // Variables necessary for managing pager and adapter
        private String titles[] = {"Unselected", "All", "Selected"};
        private int layouts[] = {R.layout.layout_list_unselected, R.layout.layout_list_all, R.layout.layout_list_selected};
        private int ids[] = {R.id.list_container_unselected, R.id.list_container_all, R.id.list_container_selected};

        CustomListPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int tabPosition) {
            LayoutInflater inflater = (LayoutInflater)
                    getApplication().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup layout = (ViewGroup) inflater.inflate(layouts[tabPosition],
                    container, false);
            container.addView(layout);

            RecyclerView rv = layout.findViewById(ids[tabPosition]);
            rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            rv.setNestedScrollingEnabled(false);
            if (tabPosition == UNSELECTED_TAB) {
                rv.setAdapter(mUnselectedAdapter);
            } else if (tabPosition == ALL_TAB) {
                rv.setAdapter(mAllAdapter);
            } else if (tabPosition == SELECTED_TAB) {
                rv.setAdapter(mSelectedAdapter);

            }
            return layout;
        }


        @Override
        public void destroyItem(ViewGroup container, int tabPosition, Object object) {
            container.removeView((View) object);
        }

        @Override
        public CharSequence getPageTitle(int tabPosition) {
            return titles[tabPosition];
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}






