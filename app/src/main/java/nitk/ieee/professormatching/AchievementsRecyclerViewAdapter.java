package nitk.ieee.professormatching;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AchievementsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "AchievementsListAdapter";
    private Context context;
    private FragmentManager fragmentManager;

    private List<Publications> publications = new ArrayList<>();
    private List<Certificates> certificates = new ArrayList<>();
    private boolean editable;

    public AchievementsRecyclerViewAdapter(Context context, FragmentManager fragmentManager, List<Publications> publications
            , List<Certificates> certificates) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.editable = editable;
        if (publications != null)
            this.publications = publications;
        if (certificates != null)
            this.certificates = certificates;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public List<Publications> getPublicationsList() {
        return this.publications;
    }

    public List<Certificates> getCertificatesList() {
        return this.certificates;
    }

    public void insertItem(Publications publicationsItem) {
        this.publications.add(publicationsItem);
        Log.d(TAG, "add Publications " + publicationsItem.getTitle());
        notifyItemInserted(this.publications.size() - 1);
    }

    public void insertItem(Certificates certificatesItem) {
        this.certificates.add(certificatesItem);
        Log.d(TAG, "add certificate " + certificatesItem.getTitle());
        notifyItemInserted(this.certificates.size() + this.publications.size() - 1);
    }

    public void changeItemAt(int position, Publications modified) {
        Publications original = publications.get(position);
        Log.d(TAG, "change publications " + original.getTitle());
        original.setTitle(modified.getTitle());
        original.setReferenceNo(modified.getReferenceNo());
        original.setJournal(modified.getJournal());
        original.setCollabs(modified.getCollabs());
        original.setMentors(modified.getMentors());
        original.setNumCitations(modified.getNumCitations());
        Log.d(TAG, "changed publications " + original.getTitle());
        notifyItemChanged(position);
    }

    public void changeItemAt(int itemPosition, Certificates modified) {
        Certificates original = this.certificates.get(itemPosition - this.publications.size());
        Log.d(TAG, "change Certificates " + original.getTitle());
        original.setTitle(modified.getTitle());
        original.setProvider(modified.getProvider());
        original.setYear(modified.getYear());
        Log.d(TAG, "changed Certificates " + original.getTitle());
        notifyItemChanged(itemPosition);
    }

    public void deletePublicationsAt(int itemPosition) {
        Log.d(TAG, "delete Publications " + itemPosition);
        this.publications.remove(itemPosition);
        notifyItemRemoved(itemPosition);
    }

    public void deleteCertificatesAt(int itemPosition) {
        Log.d(TAG, "delete Certificates" + itemPosition);
        this.certificates.remove(itemPosition - this.publications.size());
        notifyItemRemoved(itemPosition);
    }

    class ViewHolderPublications extends RecyclerView.ViewHolder {
        TextView title, referenceNo, journal, collabs, mentors, numOfCitations;

        public ViewHolderPublications(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.user_profile_achievements_publications_tv_title);
            referenceNo = itemView.findViewById(R.id.user_profile_achievements_publications_tv_reference_no);
            journal = itemView.findViewById(R.id.user_profile_achievements_publications_journal);
            collabs = itemView.findViewById(R.id.user_profile_achievements_publications_tv_collabs);
            mentors = itemView.findViewById(R.id.user_profile_achievements_publications_tv_mentors);
            numOfCitations = itemView.findViewById(R.id.user_profile_achievements_publications_citations);
        }
    }

    class ViewHolderCertificates extends RecyclerView.ViewHolder {
        TextView title, provider, year;

        public ViewHolderCertificates(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.user_profile_achievements_certificates_tv_title);
            provider = itemView.findViewById(R.id.user_profile_achievements_certificates_provider);
            year = itemView.findViewById(R.id.user_profile_achievements_certificates_tv_year);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getType " + position);
        if (position < publications.size())
            return 1;
        else
            return 2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "create " + viewType);
        switch (viewType) {
            case 1:
                View v1 = LayoutInflater.from(context)
                        .inflate(R.layout.user_profile_achievements_publications_item, parent, false);
                return new AchievementsRecyclerViewAdapter.ViewHolderPublications(v1);
            case 2:
                View v2 = LayoutInflater.from(context)
                        .inflate(R.layout.user_profile_achievements_certificates_item, parent, false);
                return new AchievementsRecyclerViewAdapter.ViewHolderCertificates(v2);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "bind " + holder.getItemViewType() + " " + position);
        switch (holder.getItemViewType()) {
            case 1:
                ViewHolderPublications localHolder1 = (ViewHolderPublications) holder;
                localHolder1.title.setText(
                        publications.get(holder.getAdapterPosition()).getTitle());
                localHolder1.referenceNo.setText(
                        publications.get(holder.getAdapterPosition()).getReferenceNo());
                localHolder1.journal.setText(
                        publications.get(holder.getAdapterPosition()).getJournal());
                localHolder1.collabs.setText(
                        publications.get(holder.getAdapterPosition()).getCollabs().toString()
                                .replace("[", "").replace("]", ""));
                localHolder1.mentors.setText(
                        publications.get(holder.getAdapterPosition()).getMentors().toString()
                                .replace("[", "").replace("]", ""));
                localHolder1.numOfCitations.setText(
                        String.valueOf(publications.get(holder.getAdapterPosition()).getNumCitations()));

                localHolder1.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AchievementsDialogFragment achievementsDialogFragment =
                                AchievementsDialogFragment.newInstance(publications.get(holder.getAdapterPosition())
                                        , editable
                                        , holder.getAdapterPosition());
                        achievementsDialogFragment.setAdapter(AchievementsRecyclerViewAdapter.this);
                        achievementsDialogFragment.show(fragmentManager, "dialog");
                    }
                });

                break;
            case 2:
                int localPosition = holder.getAdapterPosition() - publications.size();
                ViewHolderCertificates localHolder2 = (ViewHolderCertificates) holder;
                localHolder2.title.setText(certificates.get(localPosition).getTitle());
                localHolder2.provider.setText(certificates.get(localPosition).getProvider());
                localHolder2.year.setText(
                        String.valueOf(certificates.get(localPosition).getYear()));
                localHolder2.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AchievementsDialogFragment achievementsDialogFragment =
                                AchievementsDialogFragment.newInstance(certificates.get(localPosition)
                                        , editable
                                        , holder.getAdapterPosition());
                        achievementsDialogFragment.setAdapter(AchievementsRecyclerViewAdapter.this);
                        achievementsDialogFragment.show(fragmentManager, "dialog");
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return publications.size() + certificates.size();
    }
}
