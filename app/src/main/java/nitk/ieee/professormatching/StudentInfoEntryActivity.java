package nitk.ieee.professormatching;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Takes Detailed Info of Student as input and sends them to backend using volley
 */
public class StudentInfoEntryActivity extends AppCompatActivity {
    private static final String TAG = "StudentDetailEntry";
    private static final int PICK_IMAGE = 1;
    private static final int BASIC_INFO_PAGE = 0;
    private static final int DETAIL_INFO_PAGE = 1;
    private static final int WORK_PAGE = 2;
    private static final int ACHIEVEMENTS_PAGE = 3;
    private static final int OTHERS_PAGE = 4;
    StudentDetailEntryPagerAdapter studentDetailEntryPagerAdapter;
    private FloatingActionButton fab;
    private Button submit, cancel;
    private Context context;
    private ViewPager vp;
    private Bundle savedInstanceState;
    private TextInputEditText cgpa, branch, institute, yearOfStudy;
    private TextInputEditText firstName, lastName, age, telephone, city, state;
    private Spinner gender;
    private Student student;
    private boolean allowEdit;
    private LinearLayout submitLayout;
    private ProgressBar progressBar;
    private ViewPagerPageChangeListener viewPagerPageChangeListener;
    private CircleImageView profileImage;
    private String profileImageString;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("work"
                , (ArrayList<? extends Parcelable>) studentDetailEntryPagerAdapter
                        .mWorkRecyclerViewAdapter
                        .getWorkExList());
        outState.putParcelableArrayList("personalProjects"
                , (ArrayList<? extends Parcelable>) studentDetailEntryPagerAdapter
                        .mWorkRecyclerViewAdapter
                        .getPersonalProjectsList());
        outState.putParcelableArrayList("publications"
                , (ArrayList<? extends Parcelable>) studentDetailEntryPagerAdapter
                        .mAchievementsRecyclerViewAdapter
                        .getPublicationsList());
        outState.putParcelableArrayList("certificates"
                , (ArrayList<? extends Parcelable>) studentDetailEntryPagerAdapter
                        .mAchievementsRecyclerViewAdapter
                        .getCertificatesList());
        outState.putParcelable("skillsInterest"
                , studentDetailEntryPagerAdapter
                        .mOthersRecyclerViewAdapter
                        .getSkillsInterest());
        outState.putStringArrayList("hobbies"
                , (ArrayList<String>) studentDetailEntryPagerAdapter
                        .mOthersRecyclerViewAdapter
                        .getHobbiesList());
        if (cgpa != null)
            outState.putString("cgpa", cgpa.getText().toString());
        if (branch != null)
            outState.putString("branch", branch.getText().toString());
        if (institute != null)
            outState.putString("institute", institute.getText().toString());
        if (yearOfStudy != null)
            outState.putString("yearOfStudy", yearOfStudy.getText().toString());
        if (firstName != null)
            outState.putString("firstName", firstName.getText().toString());
        if (lastName != null)
            outState.putString("lastName", lastName.getText().toString());
        if (age != null)
            outState.putString("age", age.getText().toString());
        if (telephone != null)
            outState.putString("telephone", telephone.getText().toString());
        if (city != null)
            outState.putString("city", city.getText().toString());
        if (state != null)
            outState.putString("state", state.getText().toString());
        if (gender != null)
            outState.putInt("gender", gender.getSelectedItemPosition());
        outState.putBoolean("allowEdit", allowEdit);
        Log.d(TAG, "MAINTAIN LIST");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.activity_student_detail_info_entry);
        context = getApplication();
        Toolbar toolbar = findViewById(R.id.toolbar);
        submitLayout = findViewById(R.id.student_detail_info_ll_submit);
        TabLayout tabs = findViewById(R.id.student_detail_tl);
        fab = findViewById(R.id.student_detail_info_fab);
        vp = findViewById(R.id.student_detail_info_vp);
        progressBar = findViewById(R.id.student_info_progress);
        submit = findViewById(R.id.student_detail_info_btn_submit);
        cancel = findViewById(R.id.student_detail_info_btn_cancel);
        tabs.setupWithViewPager(vp);
        toolbar.setTitle("Student detail info");
        viewPagerPageChangeListener = new ViewPagerPageChangeListener();
        vp.setOffscreenPageLimit(5); // To retain information entered in first two tabs
        studentDetailEntryPagerAdapter = new StudentDetailEntryPagerAdapter();

        Bundle bundle = getIntent().getExtras();
        progressBar.setVisibility(View.VISIBLE);
        if (savedInstanceState != null) {
            Log.d(TAG, "CREATING LIST FROM SAVED INSTANCE STATE");
            studentDetailEntryPagerAdapter.setAdapter(
                    new WorkRecyclerViewAdapter(context
                            , getSupportFragmentManager()
                            , savedInstanceState.getParcelableArrayList("work")
                            , savedInstanceState.getParcelableArrayList("personalProjects")));
            studentDetailEntryPagerAdapter.setAdapter(
                    new AchievementsRecyclerViewAdapter(context
                            , getSupportFragmentManager()
                            , savedInstanceState.getParcelableArrayList("publications")
                            , savedInstanceState.getParcelableArrayList("certificates")));
            studentDetailEntryPagerAdapter.setAdapter(
                    new OthersRecyclerViewAdapter(context
                            , getSupportFragmentManager()
                            , savedInstanceState.getParcelable("skillsInterest")
                            , savedInstanceState.getStringArrayList("hobbies")));
            allowEdit = savedInstanceState.getBoolean("allowEdit");
            vp.setAdapter(studentDetailEntryPagerAdapter);
        } else if (bundle != null && bundle.getString("username") != null) {
            String studentName = bundle.getString("username");
            allowEdit = bundle.getBoolean("allowEdit");
            student = new Student();
            student.setUsername(studentName);
            toolbar.setTitle(student.getUsername());
            String basicInfoUrl = getString(R.string.URL_STUDENT_BASIC_INFO) + student.getUsername() + "/";
            String detailInfoUrl = getString(R.string.URL_STUDENT_DETAILED_INFO)
                    + student.getUsername()
                    + "/";

            JsonObjectRequest studentBasicInfoRequest = new JsonObjectRequest(
                    Request.Method.GET, basicInfoUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //#Sensitive Log
                                Log.i(TAG, "Student Basic Info " + response.toString());
                                student.studentBasicInfo =
                                        GsonHelper.getInstance(getApplicationContext())
                                                .getGson()
                                                .fromJson(response.toString(), StudentBasicInfo.class);
                                JsonObjectRequest studentDetailInfoRequest = new JsonObjectRequest(
                                        Request.Method.GET, detailInfoUrl, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject detailResponse) {
                                                try {
                                                    if (detailResponse.get("publications").equals(""))
                                                        detailResponse.remove("publications");
                                                    if (detailResponse.get("certificates").equals(""))
                                                        detailResponse.remove("certificates");
                                                    if (detailResponse.get("personalProjects").equals(""))
                                                        detailResponse.remove("personalProjects");
                                                    if (detailResponse.get("skillsInterest").equals(""))
                                                        detailResponse.remove("skillsInterest");
                                                    if (detailResponse.get("areas").equals(""))
                                                        detailResponse.remove("areas");
                                                    if (detailResponse.get("hobbies").equals(""))
                                                        detailResponse.remove("hobbies");
                                                    if (detailResponse.get("workEx").equals(""))
                                                        detailResponse.remove("workEx");
                                                    //response.remove("skillsInterest");
                                                    String serverResponse = detailResponse.toString();
                                                    serverResponse = serverResponse.replace("\\\"", "\"");
                                                    serverResponse = serverResponse.replace("\"[", "[");
                                                    serverResponse = serverResponse.replace("]\"", "]");
                                                    serverResponse = serverResponse.replace("\"{", "{");
                                                    serverResponse = serverResponse.replace("}\"", "}");
                                                    Log.i(TAG, serverResponse);
                                                    detailResponse =
                                                            (JSONObject) new JSONTokener(serverResponse).nextValue();
                                                    //#Sensitive Log
                                                    Log.i(TAG, "Student Detail Response " + detailResponse.toString());
                                                    student.studentDetailInfo =
                                                            GsonHelper.getInstance(getApplicationContext())
                                                                    .getGson()
                                                                    .fromJson(detailResponse.toString(), StudentDetailInfo.class);
                                                    studentDetailEntryPagerAdapter.setStudent(student);
                                                    studentDetailEntryPagerAdapter.setAdapter(
                                                            new WorkRecyclerViewAdapter(context
                                                                    , getSupportFragmentManager()
                                                                    , student.studentDetailInfo.getWorkEx()
                                                                    , student.studentDetailInfo.getPersonalProjects()));
                                                    studentDetailEntryPagerAdapter.setAdapter(
                                                            new AchievementsRecyclerViewAdapter(context
                                                                    , getSupportFragmentManager()
                                                                    , student.studentDetailInfo.getPublications()
                                                                    , student.studentDetailInfo.getCertificates()));
                                                    studentDetailEntryPagerAdapter.setAdapter(
                                                            new OthersRecyclerViewAdapter(context
                                                                    , getSupportFragmentManager()
                                                                    , student.studentDetailInfo.getSkillsInterest()
                                                                    , student.studentDetailInfo.getHobbies()));
                                                    vp.setAdapter(studentDetailEntryPagerAdapter);
                                                    //To disable textfields
                                                    cancel.performClick();
                                                } catch (Exception e) {
                                                    Log.d(TAG, "STUDENT DETAIL INFO RESPONSE FAILURE" + e.getMessage());
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Log.d(TAG, "STUDENT DETAIL INFO Volley FAILURE " + error.getMessage());
                                            }
                                        }) {

                                    @Override
                                    public Map<String, String> getHeaders() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("Authorization", "Token " + User.
                                                getInstance(getApplicationContext()).getToken());
                                        return params;
                                    }

                                };
                                VolleyHelper.getInstance(context).addToRequestQueue(studentDetailInfoRequest);
                            } catch (Exception e) {
                                Log.d(TAG, "STUDENT BASIC INFO RESPONSE FAILURE" + e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, "STUDENT BASIC INFO Volley FAILURE " + error.getMessage());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "Token " + User.
                            getInstance(getApplicationContext()).getToken());
                    return params;
                }

            };
            if (allowEdit) {
                toolbar.inflateMenu(R.menu.main_menu);
            }
            VolleyHelper.getInstance(context).addToRequestQueue(studentBasicInfoRequest);
        } else {
            allowEdit = true;
            studentDetailEntryPagerAdapter.setAdapter(
                    new WorkRecyclerViewAdapter(context
                            , getSupportFragmentManager()
                            , null
                            , null));
            studentDetailEntryPagerAdapter.setAdapter(
                    new AchievementsRecyclerViewAdapter(context
                            , getSupportFragmentManager()
                            , null
                            , null));
            studentDetailEntryPagerAdapter.setAdapter(
                    new OthersRecyclerViewAdapter(context
                            , getSupportFragmentManager()
                            , null
                            , null));
            vp.setAdapter(studentDetailEntryPagerAdapter);
            makeEditable();
        }
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.edit_profile) {
                    makeEditable();
                }
                return true;
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        // fab visible only when user click on edit in menu
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                if (vp.getCurrentItem() == WORK_PAGE) {
                    WorkDialogFragment workDialogFragment = WorkDialogFragment.newInstance(true);
                    workDialogFragment.setAdapter(
                            studentDetailEntryPagerAdapter.mWorkRecyclerViewAdapter);
                    workDialogFragment.show(fragmentManager, "dialog");
                } else if (vp.getCurrentItem() == ACHIEVEMENTS_PAGE) {
                    AchievementsDialogFragment achievementsDialogFragment =
                            AchievementsDialogFragment.newInstance(true);
                    achievementsDialogFragment.setAdapter(
                            studentDetailEntryPagerAdapter.mAchievementsRecyclerViewAdapter);
                    achievementsDialogFragment.show(fragmentManager, "dialog");
                } else if (vp.getCurrentItem() == OTHERS_PAGE) {
                    OthersDialogFragment othersDialogFragment =
                            OthersDialogFragment.newInstance(true);
                    othersDialogFragment.setAdapter(
                            studentDetailEntryPagerAdapter.mOthersRecyclerViewAdapter);
                    othersDialogFragment.show(fragmentManager, "dialog");
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentDetailEntryPagerAdapter.initialize();
                submitLayout.setVisibility(View.GONE);
                vp.removeOnPageChangeListener(viewPagerPageChangeListener);
                studentDetailEntryPagerAdapter.mWorkRecyclerViewAdapter.setEditable(false);
                studentDetailEntryPagerAdapter.mAchievementsRecyclerViewAdapter.setEditable(false);
                studentDetailEntryPagerAdapter.mOthersRecyclerViewAdapter.setEditable(false);
                // StudentDetailInfo fields
                {
                    cgpa.setFocusable(false);
                    cgpa.setCursorVisible(false);
                    yearOfStudy.setFocusable(false);
                    yearOfStudy.setCursorVisible(false);
                    branch.setFocusable(false);
                    branch.setCursorVisible(false);
                    institute.setFocusable(false);
                    institute.setCursorVisible(false);
                }
                // StudentBasicInfo fields
                {
                    profileImage.setOnClickListener(null);
                    firstName.setFocusable(false);
                    firstName.setCursorVisible(false);
                    lastName.setFocusable(false);
                    lastName.setCursorVisible(false);
                    age.setFocusable(false);
                    age.setCursorVisible(false);
                    telephone.setFocusable(false);
                    telephone.setCursorVisible(false);
                    city.setFocusable(false);
                    city.setCursorVisible(false);
                    state.setFocusable(false);
                    state.setCursorVisible(false);
                    gender.setEnabled(false);
                }
                fab.setVisibility(View.GONE);
            }
        });
    }

    class ViewPagerPageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            if (position == BASIC_INFO_PAGE || position == DETAIL_INFO_PAGE)
                fab.setVisibility(View.INVISIBLE);
            else
                fab.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    private void makeEditable() {
        vp.addOnPageChangeListener(viewPagerPageChangeListener);
        studentDetailEntryPagerAdapter.mWorkRecyclerViewAdapter.setEditable(true);
        studentDetailEntryPagerAdapter.mAchievementsRecyclerViewAdapter.setEditable(true);
        studentDetailEntryPagerAdapter.mOthersRecyclerViewAdapter.setEditable(true);

        // StudentDetailInfo fields
        {
            if (cgpa != null) {
                cgpa.setFocusable(true);
                cgpa.setCursorVisible(true);
                cgpa.setFocusableInTouchMode(true);
            }
            if (yearOfStudy != null) {
                yearOfStudy.setFocusable(true);
                yearOfStudy.setCursorVisible(true);
                yearOfStudy.setFocusableInTouchMode(true);
            }
            if (branch != null) {
                branch.setFocusable(true);
                branch.setCursorVisible(true);
                branch.setFocusableInTouchMode(true);
            }
            if (institute != null) {
                institute.setFocusable(true);
                institute.setCursorVisible(true);
                institute.setFocusableInTouchMode(true);
            }
        }
        // StudentBasicInfo fields
        {
            if (profileImage != null) {
                profileImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int ADD_CHANGE = 0;
                        final int DELETE = 1;
                        CharSequence colors[] =
                                new CharSequence[]{"Gallery", "Delete Image"};

                        AlertDialog.Builder builder =
                                new AlertDialog.Builder(StudentInfoEntryActivity.this);
                        builder.setTitle("Choose");
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == ADD_CHANGE) {
                                    Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                                    getIntent.setType("image/*");

                                    Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    pickIntent.setType("image/*");

                                    Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                                    startActivityForResult(chooserIntent, PICK_IMAGE);
                                } else if (which == DELETE) {
                                    profileImageString = null;
                                    GlideApp.with(context)
                                            .load(student.studentBasicInfo.getProfileImage())
                                            .placeholder(R.drawable.ic_face)
                                            .into(profileImage);
                                }
                            }
                        });
                        builder.create().show();
                    }
                });
            }
            if (firstName != null) {
                firstName.setFocusable(true);
                firstName.setCursorVisible(true);
                firstName.setFocusableInTouchMode(true);
            }
            if (lastName != null) {
                lastName.setFocusable(true);
                lastName.setCursorVisible(true);
                lastName.setFocusableInTouchMode(true);
            }
            if (age != null) {
                age.setFocusable(true);
                age.setCursorVisible(true);
                age.setFocusableInTouchMode(true);
            }
            if (telephone != null) {
                telephone.setFocusable(true);
                telephone.setCursorVisible(true);
                telephone.setFocusableInTouchMode(true);
            }
            if (city != null) {
                city.setFocusable(true);
                city.setCursorVisible(true);
                city.setFocusableInTouchMode(true);
            }
            if (state != null) {
                state.setFocusable(true);
                state.setCursorVisible(true);
                state.setFocusableInTouchMode(true);
            }
            if (gender != null) {
                gender.setEnabled(true);
            }
        }

        submitLayout.setVisibility(View.VISIBLE);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Sending basic info to server
                boolean makeServerRequest = true;
                String firstNameString = firstName.getText().toString();
                String lastNameString = lastName.getText().toString();
                String ageString = age.getText().toString();
                String telephoneString = telephone.getText().toString();
                String cityString = city.getText().toString();
                String stateString = state.getText().toString();
                String genderString = gender.getSelectedItem().toString();
                //Checks
                {
                    if (firstNameString.equals(""))
                        makeServerRequest = false;
                    if (lastNameString.equals(""))
                        makeServerRequest = false;
                    if (ageString.equals(""))
                        makeServerRequest = false;
                    if (genderString.equals(""))
                        makeServerRequest = false;
                    //TODO : more checks
                }
                if (makeServerRequest) {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("username", User.getInstance(getApplicationContext()).getUsername());
                        json.put("firstName", firstNameString);
                        json.put("lastName", lastNameString);
                        json.put("age", Integer.parseInt(ageString));
                        json.put("mobileNo", telephoneString);
                        json.put("gender", genderString);
                        json.put("city", cityString);
                        json.put("state", stateString);
                    } catch (Exception e) {
                        Log.d("ERROR", "STUDENT BASIC INFO GsonHelper CREATION FAILURE");
                    }
                    Log.d(TAG, json.toString());
                    String editUrl = getString(R.string.URL_STUDENT_BASIC_INFO) +
                            User.getInstance(getApplicationContext()).getUsername() + "/";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            Request.Method.PUT, editUrl, json,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        StudentBasicInfo studentBasicInfo =
                                                GsonHelper.getInstance(getApplicationContext())
                                                        .getGson()
                                                        .fromJson(response.toString()
                                                                , StudentBasicInfo.class);
                                        User.getInstance(getApplicationContext())
                                                .getStudent()
                                                .studentBasicInfo = studentBasicInfo;
                                    } catch (Exception e) {
                                        Log.d("ERROR", "STUDENT BASIC INFO CRUD RESPONSE FAILURE" + e.getMessage());
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("ERROR", "STUDENT BASIC INFO UPDATE CRUD FAILURE " + error.getMessage());
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Authorization", "Token " + User.
                                    getInstance(getApplicationContext()).getToken());
                            return params;
                        }

                    };
                    VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
                }

                // Sending detail info to server
                makeServerRequest = true;
                String cgpaString = cgpa.getText().toString();
                String branchString = branch.getText().toString();
                String yearOfStudyString = yearOfStudy.getText().toString();
                String instituteString = institute.getText().toString();
                {
                    if (TextUtils.isEmpty(cgpaString))
                        makeServerRequest = false;
                    if (TextUtils.isEmpty(yearOfStudyString))
                        makeServerRequest = false;
                    //TODO: more checks if required.
                }
                if (makeServerRequest) {
                    Float cgpa = Float.parseFloat(cgpaString);
                    Integer yearOfStudy = Integer.parseInt(yearOfStudyString);

                    JSONObject json = new JSONObject();
                    try {
                        //Required fields
                        json.put("username", User.getInstance(getApplicationContext()).getUsername());
                        json.put("cgpa", cgpa);
                        json.put("yearOfStudy", yearOfStudy);
                        //Optional fields
                        if (!branchString.equals(""))
                            json.put("branch", branchString);
                        if (!instituteString.equals(""))
                            json.put("institute", instituteString);
                        //Work
                        if (studentDetailEntryPagerAdapter.mWorkRecyclerViewAdapter != null
                                ) {
                            if (!studentDetailEntryPagerAdapter
                                    .mWorkRecyclerViewAdapter
                                    .getWorkExList().isEmpty()) {
                                String workEx = GsonHelper.getInstance(context)
                                        .getGson()
                                        .toJson(studentDetailEntryPagerAdapter
                                                        .mWorkRecyclerViewAdapter
                                                        .getWorkExList()
                                                , new TypeToken<ArrayList<WorkEx>>() {
                                                }.getType());
                                json.put("workEx", workEx);
                            }
                            if (!studentDetailEntryPagerAdapter
                                    .mWorkRecyclerViewAdapter
                                    .getPersonalProjectsList().isEmpty()) {
                                String personalProjects = GsonHelper.getInstance(context)
                                        .getGson()
                                        .toJson(studentDetailEntryPagerAdapter
                                                        .mWorkRecyclerViewAdapter
                                                        .getPersonalProjectsList()
                                                , new TypeToken<ArrayList<PersonalProjects>>() {
                                                }.getType());
                                json.put("personalProjects", personalProjects);
                            }
                        }
                        //Achievements
                        if (studentDetailEntryPagerAdapter.mAchievementsRecyclerViewAdapter != null
                                ) {
                            if (!studentDetailEntryPagerAdapter
                                    .mAchievementsRecyclerViewAdapter
                                    .getPublicationsList().isEmpty()) {
                                String publications = GsonHelper.getInstance(context)
                                        .getGson()
                                        .toJson(studentDetailEntryPagerAdapter
                                                        .mAchievementsRecyclerViewAdapter
                                                        .getPublicationsList()
                                                , new TypeToken<ArrayList<Publications>>() {
                                                }.getType());
                                json.put("publications", publications);
                            }
                            if (!studentDetailEntryPagerAdapter
                                    .mAchievementsRecyclerViewAdapter
                                    .getCertificatesList().isEmpty()) {
                                String certificates = GsonHelper.getInstance(context)
                                        .getGson()
                                        .toJson(studentDetailEntryPagerAdapter
                                                        .mAchievementsRecyclerViewAdapter
                                                        .getCertificatesList()
                                                , new TypeToken<ArrayList<Certificates>>() {
                                                }.getType());
                                json.put("certificates", certificates);
                            }
                        }
                        //Others
                        if (studentDetailEntryPagerAdapter.mOthersRecyclerViewAdapter != null
                                ) {
                            //TODO: Fix this
                            if (studentDetailEntryPagerAdapter
                                    .mOthersRecyclerViewAdapter
                                    .getSkillsInterest() != null) {
                                String skillsInterest = GsonHelper.getInstance(context)
                                        .getGson()
                                        .toJson(studentDetailEntryPagerAdapter
                                                .mOthersRecyclerViewAdapter
                                                .getSkillsInterest());
                                json.put("skillsInterest", skillsInterest);
                            }
                            if (!studentDetailEntryPagerAdapter
                                    .mOthersRecyclerViewAdapter
                                    .getHobbiesList().isEmpty()) {
                                String hobbies = GsonHelper.getInstance(context)
                                        .getGson()
                                        .toJson(studentDetailEntryPagerAdapter
                                                        .mOthersRecyclerViewAdapter
                                                        .getHobbiesList()
                                                , new TypeToken<ArrayList<String>>() {
                                                }.getType());
                                json.put("hobbies", hobbies);
                            }
                        }
                    } catch (Exception e) {
                        Log.d("ERROR", "STUDENT DETAIL INFO JSON OBJECT CREATION FAILED");
                    }
                    Log.d(TAG, "JSON " + json.toString());
                    String editUrl = getString(R.string.URL_STUDENT_DETAILED_INFO) +
                            User.getInstance(getApplicationContext()).getUsername() + "/";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            Request.Method.PUT, editUrl, json,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        //#SensitiveLog
                                        Log.d("SUCCESS", response.toString());
                                        if (response.get("publications").equals(""))
                                            response.remove("publications");
                                        if (response.get("certificates").equals(""))
                                            response.remove("certificates");
                                        if (response.get("personalProjects").equals(""))
                                            response.remove("personalProjects");
                                        if (response.get("skillsInterest").equals(""))
                                            response.remove("skillsInterest");
                                        if (response.get("areas").equals(""))
                                            response.remove("areas");
                                        if (response.get("hobbies").equals(""))
                                            response.remove("hobbies");
                                        if (response.get("workEx").equals(""))
                                            response.remove("workEx");
                                        //response.remove("skillsInterest");
                                        String serverResponse = response.toString();
                                        serverResponse = serverResponse.replace("\\\"", "\"");
                                        serverResponse = serverResponse.replace("\"[", "[");
                                        serverResponse = serverResponse.replace("]\"", "]");
                                        serverResponse = serverResponse.replace("\"{", "{");
                                        serverResponse = serverResponse.replace("}\"", "}");
                                        //#SensitiveLog
                                        Log.d("SUCCESS", serverResponse);
                                        response =
                                                (JSONObject) new JSONTokener(serverResponse).nextValue();
                                        StudentDetailInfo studentDetailedInfo =
                                                GsonHelper.getInstance(getApplicationContext())
                                                        .getGson()
                                                        .fromJson(response.toString()
                                                                , StudentDetailInfo.class);
                                        User.getInstance(getApplicationContext())
                                                .getStudent()
                                                .studentDetailInfo = studentDetailedInfo;
                                        Intent a = new Intent(getApplicationContext(), DashboardActivity.class);
                                        startActivity(a);
                                    } catch (Exception e) {
                                        Log.d("ERROR", "STUDENT DETAIL INFO RESPONSE FAILURE "
                                                + e.getMessage());
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("ERROR", "STUDENT DETAIL INFO UPDATE FAILURE"
                                            + error.getMessage());
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Authorization",
                                    "Token " + User.getInstance(getApplicationContext()).getToken());
                            return params;
                        }

                    };
                    VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
                } else {
                    Snackbar.make(vp, "Cgpa and Year of Study required", Snackbar.LENGTH_SHORT);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                GlideApp.with(this)
                        .load(data.getData())
                        .into(profileImage);
                Uri contentUri = data.getData();
                String[] proj = {MediaStore.Images.Media.DATA};
                CursorLoader loader = new CursorLoader(
                        getApplicationContext(),
                        contentUri,
                        proj,
                        null,
                        null,
                        null
                );
                Cursor cursor = loader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                profileImageString = cursor.getString(column_index);
                cursor.close();
            }
        }
    }

    class StudentDetailEntryPagerAdapter extends PagerAdapter {
        String[] titles = {"Basic Info", "Detail Info", "Work", "Achievements", "Others"};
        Student student;
        WorkRecyclerViewAdapter mWorkRecyclerViewAdapter;
        AchievementsRecyclerViewAdapter mAchievementsRecyclerViewAdapter;
        OthersRecyclerViewAdapter mOthersRecyclerViewAdapter;
        ArrayAdapter<CharSequence> genderAdapter = ArrayAdapter
                .createFromResource(context, R.array.gender_options,
                        android.R.layout.simple_spinner_item);
        private int[] layouts = {R.layout.student_basic_info, R.layout.student_detail_info, R.layout.user_profile_work
                , R.layout.user_profile_achievements, R.layout.user_profile_others};

        StudentDetailEntryPagerAdapter() {
        }

        public void setStudent(Student student) {
            this.student = student;
        }

        public void setAdapter(WorkRecyclerViewAdapter workRecyclerViewAdapter) {
            this.mWorkRecyclerViewAdapter = workRecyclerViewAdapter;
        }

        public void setAdapter(AchievementsRecyclerViewAdapter achievementsRecyclerViewAdapter) {
            this.mAchievementsRecyclerViewAdapter = achievementsRecyclerViewAdapter;
        }

        public void setAdapter(OthersRecyclerViewAdapter othersRecyclerViewAdapter) {
            this.mOthersRecyclerViewAdapter = othersRecyclerViewAdapter;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater1 = (LayoutInflater)
                    StudentInfoEntryActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup layout = (ViewGroup) inflater1.inflate(layouts[position], container, false);
            container.addView(layout);
            progressBar.setVisibility(View.GONE);
            if (position == BASIC_INFO_PAGE) {
                profileImage = findViewById(R.id.student_basic_info_civ_profileImage);
                firstName = findViewById(R.id.student_basic_info_tiet_firstName);
                lastName = findViewById(R.id.student_basic_info_tiet_lastName);
                age = findViewById(R.id.student_basic_info_tiet_age);
                telephone = findViewById(R.id.student_basic_info_tiet_telephone);
                city = findViewById(R.id.student_basic_info_tiet_city);
                state = findViewById(R.id.student_basic_info_tiet_state);
                gender = findViewById(R.id.student_basic_info_sp_gender);
                // Specify the layout to use when the list of choices appears
                genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                gender.setAdapter(genderAdapter);
                GlideApp.with(layout)
                        .load(student.studentBasicInfo.getProfileImage())
                        .placeholder(R.drawable.ic_face)
                        .into(profileImage);
                if (savedInstanceState != null) {
                    firstName.setText(savedInstanceState.getString("firstName"));
                    lastName.setText(savedInstanceState.getString("lastName"));
                    age.setText(savedInstanceState.getString("age"));
                    telephone.setText(savedInstanceState.getString("telephone"));
                    city.setText(savedInstanceState.getString("city"));
                    state.setText(savedInstanceState.getString("state"));
                    gender.setSelection(savedInstanceState.getInt("gender"));
                } else if (student != null) {
                    firstName.setText(student.studentBasicInfo.getFirstName());
                    lastName.setText(student.studentBasicInfo.getLastName());
                    age.setText(String.valueOf(student.studentBasicInfo.getAge()));
                    telephone.setText(student.studentBasicInfo.getMobileNo());
                    city.setText(student.studentBasicInfo.getCity());
                    state.setText(student.studentBasicInfo.getState());
                    gender.setSelection(
                            genderAdapter.getPosition(student.studentBasicInfo.getGender()));
                }
            }
            if (position == DETAIL_INFO_PAGE) {
                cgpa = findViewById(R.id.student_detail_info_tiet_cgpa);
                branch = findViewById(R.id.student_detail_info_tiet_branch);
                institute = findViewById(R.id.student_detail_info_tiet_institute);
                yearOfStudy = findViewById(R.id.student_detail_info_tiet_year_of_study);
                if (savedInstanceState != null) {
                    cgpa.setText(savedInstanceState.getString("cgpa"));
                    branch.setText(savedInstanceState.getString("branch"));
                    institute.setText(savedInstanceState.getString("institute"));
                    yearOfStudy.setText(savedInstanceState.getString("yearOfStudy"));
                } else if (student != null) {
                    cgpa.setText(String.valueOf(student.studentDetailInfo.getCgpa()));
                    branch.setText(student.studentDetailInfo.getBranch());
                    institute.setText(student.studentDetailInfo.getInstitute());
                    yearOfStudy.setText(String.valueOf(student.studentDetailInfo.getYearOfStudy()));
                }
            } else if (position == WORK_PAGE) {
                Log.d(TAG, "CREATED WorkRecyclerView ");
                RecyclerView workRecyclerView = layout.findViewById(R.id.user_profile_work_rv);
                workRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                workRecyclerView.setAdapter(mWorkRecyclerViewAdapter);
            } else if (position == ACHIEVEMENTS_PAGE) {
                Log.d(TAG, "CREATED AchievementsRecyclerView ");
                RecyclerView achievementsRecyclerView = layout.findViewById(R.id.user_profile_achievements_rv);
                achievementsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                achievementsRecyclerView.setAdapter(mAchievementsRecyclerViewAdapter);
            } else if (position == OTHERS_PAGE) {
                Log.d(TAG, "CREATED OtherRecyclerView ");
                RecyclerView othersRecyclerView = layout.findViewById(R.id.user_profile_others_rv);
                othersRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                othersRecyclerView.setAdapter(mOthersRecyclerViewAdapter);
            }
            return layout;
        }

        public void initialize() {
            firstName.setText(student.studentBasicInfo.getFirstName());
            lastName.setText(student.studentBasicInfo.getLastName());
            age.setText(String.valueOf(student.studentBasicInfo.getAge()));
            telephone.setText(student.studentBasicInfo.getMobileNo());
            city.setText(student.studentBasicInfo.getCity());
            state.setText(student.studentBasicInfo.getState());
            gender.setSelection(
                    genderAdapter.getPosition(student.studentBasicInfo.getGender()));
            cgpa.setText(String.valueOf(student.studentDetailInfo.getCgpa()));
            branch.setText(student.studentDetailInfo.getBranch());
            institute.setText(student.studentDetailInfo.getInstitute());
            yearOfStudy.setText(String.valueOf(student.studentDetailInfo.getYearOfStudy()));
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Parcelable saveState() {
            Log.d(TAG, "save state");
            return super.saveState();
        }
    }
}
