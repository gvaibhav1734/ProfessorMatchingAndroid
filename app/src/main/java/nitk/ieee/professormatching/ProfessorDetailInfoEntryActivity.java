package nitk.ieee.professormatching;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class ProfessorDetailInfoEntryActivity extends AppCompatActivity {

    private TextInputEditText minCgpa, minWorkEx, minYearOfStudy, areas, keywords, branch;
    private Button update;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("minCgpa", minCgpa.getText().toString());
        outState.putString("minWorkEx", minWorkEx.getText().toString());
        outState.putString("minYearOfStudy", minYearOfStudy.getText().toString());
        outState.putString("areas", areas.getText().toString());
        outState.putString("keywords", keywords.getText().toString());
        outState.putString("branch", branch.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professor_detail_info_entry);

        Toolbar toolbar = findViewById(R.id.professor_detail_info_tb);
        toolbar.setTitle("Professor Detail Info");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        minCgpa = findViewById(R.id.professor_detail_info_tiet_min_cgpa);
        minWorkEx = findViewById(R.id.professor_detail_info_tiet_min_work_ex);
        minYearOfStudy = findViewById(R.id.professor_detail_info_tiet_min_year_of_study);
        areas = findViewById(R.id.professor_detail_info_tiet_areas);
        keywords = findViewById(R.id.professor_detail_info_tiet_keywords);
        branch = findViewById(R.id.professor_detail_info_tiet_branch);
        update = findViewById(R.id.professor_detail_info_btn_update);

        if (savedInstanceState != null) {
            minCgpa.setText(savedInstanceState.getString("minCgpa"));
            minWorkEx.setText(savedInstanceState.getString("minWorkEx"));
            minYearOfStudy.setText(savedInstanceState.getString("minYearOfStudy"));
            areas.setText(savedInstanceState.getString("areas"));
            keywords.setText(savedInstanceState.getString("keywords"));
            branch.setText(savedInstanceState.getString("branch"));
        }
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String areasString, keywordsString, branchString;
                boolean makeServerRequest = true;
                String minWorkExperienceString = minWorkEx.getText().toString();
                String minCgpaString = minCgpa.getText().toString();
                String minYearOfStudyString = minYearOfStudy.getText().toString();
                String areasStringIntermediate = areas.getText().toString();
                String keywordsStringIntermediate = keywords.getText().toString();
                String branchStringIntermediate = branch.getText().toString();
                //Array Handling
                {
                    JSONArray areasArray = new JSONArray(), keywordsArray = new JSONArray(), branchArray = new JSONArray();
                    StringTokenizer areasTokenizer = new StringTokenizer(areasStringIntermediate, ",");
                    while (areasTokenizer.hasMoreTokens())
                        areasArray.put(areasTokenizer.nextToken());
                    StringTokenizer keywordsTokenizer = new StringTokenizer(keywordsStringIntermediate, ",");
                    while (keywordsTokenizer.hasMoreTokens())
                        keywordsArray.put(keywordsTokenizer.nextToken());
                    StringTokenizer branchTokenizer = new StringTokenizer(branchStringIntermediate, ",");
                    while (branchTokenizer.hasMoreTokens())
                        branchArray.put(branchTokenizer.nextToken());
                    areasString = areasArray.toString();
                    keywordsString = keywordsArray.toString();
                    branchString = branchArray.toString();
                }
                //Necessary Checks to determine whether the information should be passed to server.
                {
                    if (areasString.equals(""))
                        areasString = null;
                    if (keywordsString.equals(""))
                        keywordsString = null;
                    if (branchString.equals(""))
                        branchString = null;
                    if (minWorkExperienceString.equals(""))
                        makeServerRequest = false;
                    if (minCgpaString.equals(""))
                        makeServerRequest = false;
                    if (minYearOfStudyString.equals(""))
                        makeServerRequest = false;
                    //TODO : More checks and display appropriate error messages
                }
                if (makeServerRequest) {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("username", User.getInstance(getApplicationContext()).getUsername());
                        json.put("minCgpa", minCgpaString);
                        json.put("minWorkEx", minWorkExperienceString);
                        json.put("minYearOfStudy", minYearOfStudyString);
                        json.put("areas", areasString);
                        json.put("keywords", keywordsString);
                        json.put("branch", branchString);
                    } catch (Exception e) {
                        Log.d("ERROR", "PROFESSOR DETAIL INFO GsonHelper CREATION FAILURE");
                    }
                    //#SensitiveLog
                    Log.d("OUTPUT ", json.toString());
                    Log.d("OUTPUT Token ", User.getInstance(getApplicationContext()).getToken());
                    String editUrl = getString(R.string.URL_PROFESSOR_DETAILED_INFO) +
                            User.getInstance(getApplicationContext()).getUsername() + "/";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                            Request.Method.PUT, editUrl, json,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        //#SensitiveLog
                                        Log.d("OUTPUT", response.toString());
                                        ProfessorDetailedInfo professorDetailedInfo = GsonHelper.getInstance(getApplicationContext())
                                                .getGson().fromJson(response.toString(), ProfessorDetailedInfo.class);
                                        User.getInstance(getApplicationContext()).getProfessor().detailInfo = professorDetailedInfo;
                                        Intent a = new Intent(getApplicationContext(), DashboardActivity.class);
                                        startActivity(a);
                                    } catch (Exception e) {
                                        Log.d("ERROR", "PROFESSOR DETAIL INFO CRUD RESPONSE FAILURE" + e.getMessage());
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("ERROR", "PROFESSOR DETAIL INFO CRUD UPDATE FAILURE " + error.getMessage());
                                }
                            }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("Authorization", "Token " + User.
                                    getInstance(getApplicationContext()).getToken());
                            return params;
                        }

                    };
                    VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
                }
            }
        });
    }
}

