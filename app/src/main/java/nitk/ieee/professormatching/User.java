package nitk.ieee.professormatching;

import android.content.Context;

/**
 * This class contains the details and token of the user logged in...
 * Can be Student or Professor
 */
public class User
{
    private static String token;
    private static String username;
    private static String email;
    private static String type;
    private static Professor professor;
    private static Student student;
    private final Context mContext;
    private static User mInstance;

    User(Context context)
    {
        mContext = context;
    }
    public static synchronized User getInstance(Context mContext)
    {
        // If Instance is null then initialize new Instance
        if(mInstance == null){
            mInstance = new User(mContext);
            professor = new Professor();
            student = new Student();
        }
        // Return MySingleton new Instance
        return mInstance;
    }

    public void logout() {
        mInstance=null;
    }

    public Student getStudent() {
        return student;
    }


    public String getType() {
        return type;
    }

    public static void setType(String type) {
        User.type = type;
    }

    public Professor getProfessor() {
        return professor;
    }


    public void setData(String username,String email,String token,String type)
    {
        setUsername(username);
        setEmail(email);
        setToken(token);
        setType(type);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
