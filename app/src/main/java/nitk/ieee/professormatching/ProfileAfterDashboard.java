package nitk.ieee.professormatching;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;


public class ProfileAfterDashboard extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ProfilePageAdapter profilePageAdapter;
    TabItem basicInfo;
    TabItem detailedInfo;
    private LinearLayout llSubmit;
    private Button submit, cancel;


  @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.profile_dashboard);
        Toolbar toolbar = findViewById(R.id.profile_dashboard_tb);
        toolbar.setTitle("Profile");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        //setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.edit_profile:
                        submitEdit();

                }
                return true;
            }
        });
        tabLayout = findViewById(R.id.tablayout);
        basicInfo = findViewById(R.id.basicinfo_profile);
        detailedInfo = findViewById(R.id.detailedinfo_profile);
        viewPager = findViewById(R.id.viewPager);
        submit = findViewById(R.id.submit);
        cancel = findViewById(R.id.cancel);
        llSubmit = findViewById(R.id.student_detail_info_ll_submit);

        profilePageAdapter = new ProfilePageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(profilePageAdapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


    }

    private void submitEdit() {
        llSubmit.setVisibility(View.VISIBLE);
        FragmentManager manager = getSupportFragmentManager();
        BasicinfoFragment basicinfoFragment = (BasicinfoFragment) manager.getFragments().get(0);
        DetailInfoFragment detailinfoFragment = (DetailInfoFragment) manager.getFragments().get(1);
        basicinfoFragment.editable();
        detailinfoFragment.editable();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             basicinfoFragment.update();
             detailinfoFragment.update();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailinfoFragment.noneditable();
                basicinfoFragment.nonEditable();
                llSubmit.setVisibility(View.GONE);
                basicinfoFragment.initialize();
                detailinfoFragment.initialize();
            }
        });

    }

    public class ProfilePageAdapter extends FragmentPagerAdapter {

        private int numOfTabs;

        ProfilePageAdapter(FragmentManager fm, int numOfTabs) {
            super(fm);
            this.numOfTabs = numOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    BasicinfoFragment basicinfoFragment = new BasicinfoFragment();
                    //getSupportFragmentManager().beginTransaction().add(basicinfoFragment ,"0").commit();
                    return basicinfoFragment;
                case 1:
                    DetailInfoFragment detailInfoFragment = new DetailInfoFragment();
                    //getSupportFragmentManager().beginTransaction().add(detailInfoFragment ,"1").commit();
                    return detailInfoFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return numOfTabs;
        }
    }

}
