package nitk.ieee.professormatching;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Modal class to store Student
 */

public class Student implements Parcelable{
    static StudentBasicInfo studentBasicInfo = new StudentBasicInfo();
    static StudentDetailInfo studentDetailInfo = new StudentDetailInfo();
    private String username;
    private boolean selected;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return username + studentBasicInfo.getFirstName() + studentDetailInfo.getCgpa();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeParcelable(studentBasicInfo,flags);
        dest.writeParcelable(studentDetailInfo,flags);
    }

    public final static Parcelable.Creator<Student> CREATOR =
            new Parcelable.Creator<Student>() {
                @Override
                public Student createFromParcel(Parcel source) {
                    Student student = new Student();
                    student.setUsername(source.readString());
                    student.studentBasicInfo = source.readParcelable(StudentBasicInfo.class.getClassLoader());
                    student.studentDetailInfo = source.readParcelable(StudentDetailInfo.class.getClassLoader());
                    return student;
                }

                @Override
                public Student[] newArray(int size) {
                    return new Student[0];
                }
            };

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public boolean equals(Object object) {
        if (object != null && object instanceof Student) {
            return username.equals(((Student) object).getUsername());
        }
        return false;
    }
}

class StudentBasicInfo implements Parcelable{
    //Basic Info
    private String firstName, lastName, gender, mobileNo, email;
    private int age;
    private String city, state;
    private String profileImage;

    StudentBasicInfo() {

    }

    //Getters and Setters.
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(gender);
        dest.writeString(mobileNo);
        dest.writeString(email);
        dest.writeInt(age);
        dest.writeString(city);
        dest.writeString(state);
    }

    public static final Parcelable.Creator<StudentBasicInfo> CREATOR =
            new Parcelable.Creator<StudentBasicInfo>() {
                @Override
                public StudentBasicInfo createFromParcel(Parcel source) {
                    StudentBasicInfo studentBasicInfo = new StudentBasicInfo();
                    studentBasicInfo.setFirstName(source.readString());
                    studentBasicInfo.setLastName(source.readString());
                    studentBasicInfo.setGender(source.readString());
                    studentBasicInfo.setMobileNo(source.readString());
                    studentBasicInfo.setEmail(source.readString());
                    studentBasicInfo.setAge(source.readInt());
                    studentBasicInfo.setCity(source.readString());
                    studentBasicInfo.setState(source.readString());
                    return studentBasicInfo;
                }

                @Override
                public StudentBasicInfo[] newArray(int size) {
                    return new StudentBasicInfo[0];
                }
            };

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}

class StudentDetailInfo implements Parcelable{
    // Detailed Info
    private String institute, branch;
    private int yearOfStudy;
    private float cgpa;
    private List<WorkEx> workEx;
    private List<PersonalProjects> personalProjects;
    private List<Publications> publications;
    private List<Certificates> certificates;
    public final static Parcelable.Creator<StudentDetailInfo> CREATOR =
            new Parcelable.Creator<StudentDetailInfo>() {
                @Override
                public StudentDetailInfo createFromParcel(Parcel source) {
                    StudentDetailInfo studentDetailInfo = new StudentDetailInfo();
                    studentDetailInfo.setBranch(source.readString());
                    studentDetailInfo.setInstitute(source.readString());
                    studentDetailInfo.setYearOfStudy(source.readInt());
                    studentDetailInfo.setCgpa(source.readFloat());
                    List<WorkEx> workExes = new ArrayList<>();
                    List<PersonalProjects> personalProjects = new ArrayList<>();
                    List<Publications> publications = new ArrayList<>();
                    List<Certificates> certificates = new ArrayList<>();
                    List<String> hobbies = new ArrayList<>();
                    List<String> areas = new ArrayList<>();
                    source.readList(workExes, WorkEx.class.getClassLoader());
                    source.readList(personalProjects, PersonalProjects.class.getClassLoader());
                    source.readList(publications, Publications.class.getClassLoader());
                    source.readList(certificates, Certificates.class.getClassLoader());
                    source.readList(hobbies, String.class.getClassLoader());
                    source.readList(areas, String.class.getClassLoader());
                    studentDetailInfo.setWorkEx(workExes);
                    studentDetailInfo.setPersonalProjects(personalProjects);
                    studentDetailInfo.setPublications(publications);
                    studentDetailInfo.setCertificates(certificates);
                    studentDetailInfo.setSkillsInterest(source.readParcelable(SkillsInterest.class.getClassLoader()));
                    studentDetailInfo.setHobbies(hobbies);
                    studentDetailInfo.setAreas(areas);
                    return studentDetailInfo;
                }

                @Override
                public StudentDetailInfo[] newArray(int size) {
                    return new StudentDetailInfo[0];
                }
            };
    private List<String> hobbies, areas;

    StudentDetailInfo() {

    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public float getCgpa() {
        return cgpa;
    }

    public void setCgpa(float cgpa) {
        this.cgpa = cgpa;
    }

    public List<WorkEx> getWorkEx() {
        return workEx;
    }

    public void setWorkEx(List<WorkEx> workEx) {
        this.workEx = workEx;
    }


    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public List<String> getAreas() {
        return areas;
    }

    public void setAreas(List<String> areas) {
        this.areas = areas;
    }

    public List<PersonalProjects> getPersonalProjects() {
        return personalProjects;
    }

    public void setPersonalProjects(List<PersonalProjects> personalProjects) {
        this.personalProjects = personalProjects;
    }

    public List<Publications> getPublications() {
        return publications;
    }

    public void setPublications(List<Publications> publications) {
        this.publications = publications;
    }

    public List<Certificates> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificates> certificates) {
        this.certificates = certificates;
    }

    private SkillsInterest skillsInterest;

    public SkillsInterest getSkillsInterest() {
        return skillsInterest;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void setSkillsInterest(SkillsInterest skillsInterest) {
        this.skillsInterest = skillsInterest;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(institute);
        dest.writeString(branch);
        dest.writeInt(yearOfStudy);
        dest.writeFloat(cgpa);
        dest.writeList(workEx);
        dest.writeList(personalProjects);
        dest.writeList(publications);
        dest.writeList(certificates);
        dest.writeParcelable(skillsInterest, 1);
        dest.writeList(hobbies);
        dest.writeList(areas);
    }
}

class WorkEx implements Parcelable {
    String organization, description;
    int duration;

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(organization);
        dest.writeInt(duration);
        dest.writeString(description);
    }

    public static final Parcelable.Creator<WorkEx> CREATOR
            = new Parcelable.Creator<WorkEx>() {
        @Override
        public WorkEx createFromParcel(Parcel source) {
            WorkEx workEx = new WorkEx();
            workEx.setOrganization(source.readString());
            workEx.setDuration(source.readInt());
            workEx.setDescription(source.readString());
            return workEx;
        }

        @Override
        public WorkEx[] newArray(int size) {
            return new WorkEx[0];
        }
    };
}

class Publications implements Parcelable{
    private String title, referenceNo, journal,description;
    private List<String> collabs, mentors;
    private int numCitations;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }


    public int getNumCitations() {
        return numCitations;
    }

    public void setNumCitations(int numCitations) {
        this.numCitations = numCitations;
    }

    public List<String> getCollabs() {
        return collabs;
    }

    public void setCollabs(List<String> collabs) {
        this.collabs = collabs;
    }

    public List<String> getMentors() {
        return mentors;
    }

    public void setMentors(List<String> mentors) {
        this.mentors = mentors;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(referenceNo);
        dest.writeString(journal);
        dest.writeList(collabs);
        dest.writeList(mentors);
        dest.writeInt(numCitations);
        dest.writeString(description);
    }

    public static final Parcelable.Creator<Publications> CREATOR =
            new Parcelable.Creator<Publications>() {
                @Override
                public Publications createFromParcel(Parcel source) {
                    Publications publications = new Publications();
                    publications.setTitle(source.readString());
                    publications.setReferenceNo(source.readString());
                    publications.setJournal(source.readString());
                    List<String> collabs = new ArrayList<>();
                    List<String> mentors = new ArrayList<>();
                    source.readList(collabs,String.class.getClassLoader());
                    source.readList(mentors,String.class.getClassLoader());
                    publications.setCollabs(collabs);
                    publications.setMentors(mentors);
                    publications.setNumCitations(source.readInt());
                    publications.setDescription(source.readString());
                    return publications;
                }

                @Override
                public Publications[] newArray(int size) {
                    return new Publications[0];
                }
            };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

class Certificates implements Parcelable{
    private String title, provider;
    private int year;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(provider);
        dest.writeInt(year);
    }
    public static final Parcelable.Creator<Certificates> CREATOR =
            new Parcelable.Creator<Certificates>() {
                @Override
                public Certificates createFromParcel(Parcel source) {
                    Certificates certificates = new Certificates();
                    certificates.setTitle(source.readString());
                    certificates.setProvider(source.readString());
                    certificates.setYear(source.readInt());
                    return certificates;
                }

                @Override
                public Certificates[] newArray(int size) {
                    return new Certificates[0];
                }
            };
}

class PersonalProjects implements Parcelable {
    private String title, description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
    }

    public static final Parcelable.Creator<PersonalProjects> CREATOR =
            new Parcelable.Creator<PersonalProjects>() {
                @Override
                public PersonalProjects createFromParcel(Parcel source) {
                    PersonalProjects personalProjects = new PersonalProjects();
                    personalProjects.setTitle(source.readString());
                    personalProjects.setDescription(source.readString());
                    return personalProjects;
                }

                @Override
                public PersonalProjects[] newArray(int size) {
                    return new PersonalProjects[0];
                }
            };
}

class SkillsInterest implements Parcelable{
    private List<String> areas, keywords;

    public List<String> getAreas() {
        return areas;
    }

    public void setAreas(List<String> areas) {
        this.areas = areas;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(areas);
        dest.writeList(keywords);
    }

    public static final Parcelable.Creator<SkillsInterest> CREATOR =
            new Parcelable.Creator<SkillsInterest>() {
                @Override
                public SkillsInterest createFromParcel(Parcel source) {
                    SkillsInterest skillsInterest = new SkillsInterest();
                    List<String> areas = new ArrayList<>();
                    List<String> keywords = new ArrayList<>();
                    source.readList(areas,String.class.getClassLoader());
                    source.readList(keywords,String.class.getClassLoader());
                    return skillsInterest;
                }

                @Override
                public SkillsInterest[] newArray(int size) {
                    return new SkillsInterest[0];
                }
            };
}