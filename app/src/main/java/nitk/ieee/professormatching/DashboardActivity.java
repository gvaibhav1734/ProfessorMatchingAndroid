package nitk.ieee.professormatching;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {

    CardView profile;
    CardView list;
    CardView settings;
    CardView logout;
    AlertDialog alert;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        Toolbar toolbar = findViewById(R.id.dashboard_tb);
        toolbar.setTitle(getString(R.string.app_name));

        profile=(CardView)findViewById(R.id.profile);
        list=(CardView)findViewById(R.id.list);
        settings=(CardView)findViewById(R.id.settings);
        logout=(CardView)findViewById(R.id.logout);
        profile.setOnClickListener(this);
        list.setOnClickListener(this);
        settings.setOnClickListener(this);
        logout.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.profile:
            {   Intent p;
                if (User.getInstance(this).getType().equals("prof")) {
                     p = new Intent(getApplicationContext(), ProfileAfterDashboard.class);

                }
                else{
                     p= new Intent(getApplicationContext(), StudentInfoEntryActivity.class);
                    p.putExtra("username", User.getInstance(getApplicationContext()).getUsername());
                    p.putExtra("allowEdit",true);

                }
                startActivity(p);
                break;
            }

            case R.id.settings:
            {
                Intent s=new Intent(getApplicationContext(),SettingsActivity.class);
                startActivity(s);
                break;
            }

            case R.id.logout:
            {
                Details();
                break;
            }
            case R.id.list:
            {
                Intent l = new Intent(getApplicationContext(),ListContainerActivity.class);
                startActivity(l);
                break;
            }

        }
    }



    private void Details(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogStyle);

        builder.setMessage("Do want to logout?")
                .setCancelable(false)
                .setPositiveButton( getResources().getString(R.string.yes), new DialogInterface.OnClickListener()
                        {
                            public void onClick(final DialogInterface dialog, int id) {
                                User.getInstance(getApplicationContext()).logout();
                                Intent i = getBaseContext().getPackageManager().
                                        getLaunchIntentForPackage(getBaseContext().getPackageName());
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                                System.exit(0);
                            }
                        }
                )
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        //dialog box details
        alert = builder.create();
        alert.setTitle(" EXIT APP ");
        alert.show();
    }

    @Override
    public void onBackPressed() {
        Details();
    }
}


