package nitk.ieee.professormatching;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class WorkDialogFragment extends DialogFragment {
    private static final String TAG = "WorkDialog";
    private WorkEx workEx;
    private PersonalProjects personalProjects;
    private boolean editable;
    private int layouts[] = {R.layout.dialog_work_work_ex, R.layout.dialog_work_personal_projects};
    private int itemPosition;
    private Button addSaveButton;
    private RelativeLayout deleteButton;
    private ViewPager viewPager;
    private WorkRecyclerViewAdapter workRecyclerViewAdapter;
    private EditText organization, duration, description, title, descriptionWorkEx;
    private Bundle savedInstanceState;

    public WorkDialogFragment() {
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "Saving instance state");
        super.onSaveInstanceState(outState);
        if (organization != null)
            outState.putString("organization", organization.getText().toString());
        if (duration != null)
            outState.putString("duration", duration.getText().toString());
        if (descriptionWorkEx != null)
            outState.putString("descriptionWorkEx", descriptionWorkEx.getText().toString());
        if (title != null)
            outState.putString("title", title.getText().toString());
        if (description != null)
            outState.putString("description", description.getText().toString());
    }

    public void setAdapter(WorkRecyclerViewAdapter workRecyclerViewAdapter) {
        this.workRecyclerViewAdapter = workRecyclerViewAdapter;
    }

    static WorkDialogFragment newInstance(WorkEx workEx, boolean editable, int itemPosition) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("workEx", workEx);
        bundle.putBoolean("editable", editable);
        bundle.putInt("position", itemPosition);
        WorkDialogFragment workDialogFragment = new WorkDialogFragment();
        workDialogFragment.setArguments(bundle);
        return workDialogFragment;
    }

    static WorkDialogFragment newInstance(PersonalProjects personalProjects, boolean editable, int itemPosition) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("personalProjects", personalProjects);
        bundle.putBoolean("editable", editable);
        bundle.putInt("position", itemPosition);
        WorkDialogFragment workDialogFragment = new WorkDialogFragment();
        workDialogFragment.setArguments(bundle);
        return workDialogFragment;
    }

    static WorkDialogFragment newInstance(boolean editable) {
        WorkDialogFragment workDialogFragment = new WorkDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("editable", editable);
        workDialogFragment.setArguments(bundle);
        return workDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "FRAGMENT");
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.get("workEx") != null) {
                workEx = bundle.getParcelable("workEx");
                editable = bundle.getBoolean("editable");
                itemPosition = bundle.getInt("position");
                layouts = new int[1];
                layouts[0] = R.layout.dialog_work_work_ex;
            } else if (bundle.get("personalProjects") != null) {
                personalProjects = bundle.getParcelable("personalProjects");
                editable = bundle.getBoolean("editable");
                itemPosition = bundle.getInt("position");
                layouts = new int[1];
                layouts[0] = R.layout.dialog_work_personal_projects;
            } else {
                editable = bundle.getBoolean("editable");
            }
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.dialog_work, container, false);
        viewPager = rootView.findViewById(R.id.dialog_work_vp);
        TabLayout tabLayout = rootView.findViewById(R.id.dialog_work_tl);
        addSaveButton = rootView.findViewById(R.id.dialog_work_btn_add_save);
        deleteButton = rootView.findViewById(R.id.dialog_work_delete);
        if (workEx != null || personalProjects != null) {
            tabLayout.setVisibility(View.GONE);
            if (editable)
                deleteButton.setVisibility(View.VISIBLE);
        }
        if (!editable) {
            deleteButton.setVisibility(View.GONE);
            addSaveButton.setVisibility(View.GONE);
        }
        viewPager.setAdapter(new WorkDialogAdapter());
        if (savedInstanceState != null)
            this.savedInstanceState = savedInstanceState;
        tabLayout.setupWithViewPager(viewPager);
        return rootView;
    }

    class WorkDialogAdapter extends PagerAdapter {
        private static final String TAG = "WorkDialogAdapter";
        String titles[] = {"Work Experience", "Personal Projects"};

        WorkDialogAdapter() {

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup rootViewDialog = (ViewGroup) inflater.inflate(layouts[position], container, false);
            container.addView(rootViewDialog);

            Log.d(TAG, "Initializing fields");
            organization = getDialog().findViewById(R.id.dialog_work_ex_organization);
            duration = getDialog().findViewById(R.id.dialog_work_ex_duration);
            descriptionWorkEx = getDialog().findViewById(R.id.dialog_work_ex_description);
            title = getDialog().findViewById(R.id.dialog_personal_projects_title);
            description = getDialog().findViewById(R.id.dialog_personal_projects_description);

            if (savedInstanceState != null) {
                Log.d(TAG, "Creating from savedInstanceState");
                if (organization != null)
                    organization.setText(savedInstanceState.getString("organization"));
                if (duration != null)
                    duration.setText(savedInstanceState.getString("duration"));
                if (descriptionWorkEx != null)
                    descriptionWorkEx.setText(savedInstanceState.getString("descriptionWorkEx"));
                if (title != null)
                    title.setText(savedInstanceState.getString("title"));
                if (description != null)
                    description.setText(savedInstanceState.getString("description"));
                if (getActivity() instanceof StudentInfoEntryActivity)
                    workRecyclerViewAdapter = ((StudentInfoEntryActivity) getActivity())
                            .studentDetailEntryPagerAdapter
                            .mWorkRecyclerViewAdapter;
            }
            if (workEx != null) {
                organization.setText(workEx.getOrganization());
                duration.setText(String.valueOf(workEx.getDuration()));
                descriptionWorkEx.setText(workEx.getDescription());
                if (editable) {
                    addSaveButton.setText("SAVE");
                    addSaveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            WorkEx workEx = new WorkEx();
                            workEx.setOrganization(organization.getText().toString());
                            workEx.setDuration(Integer.parseInt(duration.getText().toString()));
                            workEx.setDescription(descriptionWorkEx.getText().toString());
                            workRecyclerViewAdapter.changeItemAt(itemPosition, workEx);
                            getDialog().dismiss();
                        }
                    });
                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            workRecyclerViewAdapter.deleteWorkExAt(itemPosition);
                            getDialog().dismiss();
                        }
                    });
                }
                if (!editable) {
                    organization.setFocusable(editable);
                    organization.setCursorVisible(editable);
                    organization.setBackgroundColor(Color.TRANSPARENT);

                    duration.setFocusable(editable);
                    duration.setCursorVisible(editable);
                    duration.setBackgroundColor(Color.TRANSPARENT);

                    descriptionWorkEx.setFocusable(editable);
                    descriptionWorkEx.setCursorVisible(editable);
                    descriptionWorkEx.setBackgroundColor(Color.TRANSPARENT);
                }
            } else if (personalProjects != null) {
                title.setText(personalProjects.getTitle());
                description.setText(personalProjects.getDescription());
                if (editable) {
                    addSaveButton.setText("SAVE");
                    addSaveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PersonalProjects workEx = new PersonalProjects();
                            personalProjects.setTitle(title.getText().toString());
                            personalProjects.setDescription(description.getText().toString());
                            workRecyclerViewAdapter.changeItemAt(itemPosition, personalProjects);
                            getDialog().dismiss();
                        }
                    });

                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            workRecyclerViewAdapter.deletePersonalProjectsAt(itemPosition);
                            getDialog().dismiss();
                        }
                    });
                }
                if (!editable) {
                    title.setFocusable(editable);
                    title.setCursorVisible(editable);
                    title.setBackgroundColor(Color.TRANSPARENT);

                    description.setFocusable(editable);
                    description.setCursorVisible(editable);
                    description.setBackgroundColor(Color.TRANSPARENT);
                }
            } else {
                //Add click listener to common button.
                addSaveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewPager.getCurrentItem() == 0) {
                            WorkEx workEx = new WorkEx();
                            workEx.setOrganization(organization.getText().toString());
                            workEx.setDuration(Integer.valueOf(duration.getText().toString()));
                            workEx.setDescription(descriptionWorkEx.getText().toString());
                            workRecyclerViewAdapter.insertItem(workEx);
                            organization.setText("");
                            duration.setText("");
                            descriptionWorkEx.setText("");
                        } else if (viewPager.getCurrentItem() == 1) {
                            PersonalProjects personalProjects = new PersonalProjects();
                            personalProjects.setTitle(title.getText().toString());
                            personalProjects.setDescription(description.getText().toString());
                            workRecyclerViewAdapter.insertItem(personalProjects);
                            title.setText("");
                            description.setText("");
                        }
                    }
                });
            }
            return rootViewDialog;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
