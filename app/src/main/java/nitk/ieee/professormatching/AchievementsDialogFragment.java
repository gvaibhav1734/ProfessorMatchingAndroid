package nitk.ieee.professormatching;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class AchievementsDialogFragment extends DialogFragment {
    private static final String TAG = "AchievementsDialog";
    private Publications publications;
    private Certificates certificates;
    private boolean editable;
    private int layouts[] = {R.layout.dialog_achievements_publications, R.layout.dialog_achievements_certificates};
    private int itemPosition;
    private Button addSaveButton;
    private RelativeLayout deleteButton;
    private ViewPager viewPager;
    private AchievementsRecyclerViewAdapter achievementsRecyclerViewAdapter;
    private EditText title, referenceNo, journal, collabs, mentors, numOfCitations, description;
    private EditText titleCertificate, provider, year;
    private Bundle savedInstanceState;

    public AchievementsDialogFragment() {
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "Saving instance state");
        super.onSaveInstanceState(outState);
        if (title != null)
            outState.putString("title", title.getText().toString());
        if (referenceNo != null)
            outState.putString("referenceNo", referenceNo.getText().toString());
        if (journal != null)
            outState.putString("journal", journal.getText().toString());
        if (collabs != null)
            outState.putString("collabs", collabs.getText().toString());
        if (mentors != null)
            outState.putString("mentors", mentors.getText().toString());
        if (description != null)
            outState.putString("description", description.getText().toString());
        if (numOfCitations != null)
            outState.putString("numOfCitations", numOfCitations.getText().toString());
        if (titleCertificate != null)
            outState.putString("titleCertificate", titleCertificate.getText().toString());
        if (provider != null)
            outState.putString("provider", provider.getText().toString());
        if (year != null)
            outState.putString("year", year.getText().toString());
    }

    public void setAdapter(AchievementsRecyclerViewAdapter achievementsRecyclerViewAdapter) {
        this.achievementsRecyclerViewAdapter = achievementsRecyclerViewAdapter;
    }

    static AchievementsDialogFragment newInstance(Publications publications, boolean editable, int itemPosition) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("publications", publications);
        bundle.putBoolean("editable", editable);
        bundle.putInt("position", itemPosition);
        AchievementsDialogFragment achievementsDialogFragment = new AchievementsDialogFragment();
        achievementsDialogFragment.setArguments(bundle);
        return achievementsDialogFragment;
    }

    static AchievementsDialogFragment newInstance(Certificates certificates, boolean editable, int itemPosition) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("certificates", certificates);
        bundle.putBoolean("editable", editable);
        bundle.putInt("position", itemPosition);
        AchievementsDialogFragment achievementsDialogFragment = new AchievementsDialogFragment();
        achievementsDialogFragment.setArguments(bundle);
        return achievementsDialogFragment;
    }

    static AchievementsDialogFragment newInstance(boolean editable) {
        AchievementsDialogFragment achievementsDialogFragment = new AchievementsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("editable", editable);
        achievementsDialogFragment.setArguments(bundle);
        return achievementsDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "FRAGMENT");
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.get("publications") != null) {
                publications = bundle.getParcelable("publications");
                editable = bundle.getBoolean("editable");
                itemPosition = bundle.getInt("position");
                layouts = new int[1];
                layouts[0] = R.layout.dialog_achievements_publications;
            } else if (bundle.get("certificates") != null) {
                certificates = bundle.getParcelable("certificates");
                editable = bundle.getBoolean("editable");
                itemPosition = bundle.getInt("position");
                layouts = new int[1];
                layouts[0] = R.layout.dialog_achievements_certificates;
            } else {
                editable = bundle.getBoolean("editable");
            }
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.dialog_achievements, container, false);
        viewPager = rootView.findViewById(R.id.dialog_achievements_vp);
        TabLayout tabLayout = rootView.findViewById(R.id.dialog_achievements_tl);
        addSaveButton = rootView.findViewById(R.id.dialog_achievements_btn_add_save);
        deleteButton = rootView.findViewById(R.id.dialog_achievements_delete);
        if (publications != null || certificates != null) {
            tabLayout.setVisibility(View.GONE);
            if (editable)
                deleteButton.setVisibility(View.VISIBLE);
        }
        if (!editable) {
            deleteButton.setVisibility(View.GONE);
            addSaveButton.setVisibility(View.GONE);
        }
        viewPager.setAdapter(new AchievementsDialogAdapter());
        if (savedInstanceState != null)
            this.savedInstanceState = savedInstanceState;
        tabLayout.setupWithViewPager(viewPager);
        return rootView;
    }

    class AchievementsDialogAdapter extends PagerAdapter {
        private static final String TAG = "AchievementsAdapter";
        String titles[] = {"Publications", "Certificates"};

        AchievementsDialogAdapter() {

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup rootViewDialog = (ViewGroup) inflater.inflate(layouts[position], container, false);
            container.addView(rootViewDialog);

            Log.d(TAG, "Initializing fields");
            title = getDialog().findViewById(R.id.dialog_publications_title);
            referenceNo = getDialog().findViewById(R.id.dialog_publications_reference_no);
            journal = getDialog().findViewById(R.id.dialog_publications_journal);
            collabs = getDialog().findViewById(R.id.dialog_publications_collabs);
            mentors = getDialog().findViewById(R.id.dialog_publications_mentors);
            description = getDialog().findViewById(R.id.dialog_publications_description);
            numOfCitations = getDialog().findViewById(R.id.dialog_publications_citations);
            titleCertificate = getDialog().findViewById(R.id.dialog_certificates_title);
            provider = getDialog().findViewById(R.id.dialog_certificates_provider);
            year = getDialog().findViewById(R.id.dialog_certificates_year);

            if (savedInstanceState != null) {
                Log.d(TAG, "Creating from savedInstanceState");
                if (title != null)
                    title.setText(savedInstanceState.getString("title"));
                if (referenceNo != null)
                    referenceNo.setText(savedInstanceState.getString("referenceNo"));
                if (journal != null)
                    journal.setText(savedInstanceState.getString("journal"));
                if (collabs != null)
                    collabs.setText(savedInstanceState.getString("collabs"));
                if (mentors != null)
                    mentors.setText(savedInstanceState.getString("mentors"));
                if (description != null)
                    description.setText(savedInstanceState.getString("description"));
                if (numOfCitations != null)
                    numOfCitations.setText(savedInstanceState.getString("numOfCitations"));
                if (titleCertificate != null)
                    titleCertificate.setText(savedInstanceState.getString("titleCertificate"));
                if (provider != null)
                    provider.setText(savedInstanceState.getString("provider"));
                if (year != null)
                    year.setText(savedInstanceState.getString("year"));
                if (getActivity() instanceof StudentInfoEntryActivity)
                    achievementsRecyclerViewAdapter = ((StudentInfoEntryActivity) getActivity())
                            .studentDetailEntryPagerAdapter
                            .mAchievementsRecyclerViewAdapter;
            }
            if (publications != null) {
                title.setText(publications.getTitle());
                referenceNo.setText(publications.getReferenceNo());
                journal.setText(publications.getJournal());
                collabs.setText(publications.getCollabs().toString()
                        .replace("[", "").replace("]", ""));
                mentors.setText(publications.getMentors().toString()
                        .replace("[", "").replace("]", ""));
                description.setText(publications.getDescription());
                numOfCitations.setText(String.valueOf(publications.getNumCitations()));
                if (editable) {
                    addSaveButton.setText("SAVE");
                    addSaveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Publications publications = new Publications();
                            publications.setTitle(title.getText().toString());
                            publications.setReferenceNo(referenceNo.getText().toString());
                            publications.setJournal(journal.getText().toString());
                            List<String> collabsList = makeList(collabs.getText().toString());
                            List<String> mentorsList = makeList(mentors.getText().toString());
                            publications.setCollabs(collabsList);
                            publications.setMentors(mentorsList);
                            publications.setDescription(description.getText().toString());
                            publications.setNumCitations(
                                    Integer.parseInt(numOfCitations.getText().toString()));
                            achievementsRecyclerViewAdapter.changeItemAt(itemPosition, publications);
                            getDialog().dismiss();
                        }
                    });
                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            achievementsRecyclerViewAdapter.deletePublicationsAt(itemPosition);
                            getDialog().dismiss();
                        }
                    });
                }
                if (!editable) {
                    title.setFocusable(editable);
                    title.setCursorVisible(editable);
                    title.setBackgroundColor(Color.TRANSPARENT);

                    referenceNo.setFocusable(editable);
                    referenceNo.setCursorVisible(editable);
                    referenceNo.setBackgroundColor(Color.TRANSPARENT);

                    journal.setFocusable(editable);
                    journal.setCursorVisible(editable);
                    journal.setBackgroundColor(Color.TRANSPARENT);

                    collabs.setFocusable(editable);
                    collabs.setCursorVisible(editable);
                    collabs.setBackgroundColor(Color.TRANSPARENT);

                    mentors.setFocusable(editable);
                    mentors.setCursorVisible(editable);
                    mentors.setBackgroundColor(Color.TRANSPARENT);

                    description.setFocusable(editable);
                    description.setCursorVisible(editable);
                    description.setBackgroundColor(Color.TRANSPARENT);

                    numOfCitations.setFocusable(editable);
                    numOfCitations.setCursorVisible(editable);
                    numOfCitations.setBackgroundColor(Color.TRANSPARENT);
                }
            } else if (certificates != null) {
                titleCertificate.setText(certificates.getTitle());
                provider.setText(certificates.getProvider());
                year.setText(String.valueOf(certificates.getYear()));
                if (editable) {
                    addSaveButton.setText("SAVE");
                    addSaveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Certificates certificates = new Certificates();
                            certificates.setTitle(titleCertificate.getText().toString());
                            certificates.setProvider(provider.getText().toString());
                            certificates.setYear(Integer.parseInt(year.getText().toString()));
                            achievementsRecyclerViewAdapter.changeItemAt(itemPosition, certificates);
                            getDialog().dismiss();
                        }
                    });

                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            achievementsRecyclerViewAdapter.deleteCertificatesAt(itemPosition);
                            getDialog().dismiss();
                        }
                    });
                }
                if (!editable) {
                    titleCertificate.setFocusable(editable);
                    titleCertificate.setCursorVisible(editable);
                    titleCertificate.setBackgroundColor(Color.TRANSPARENT);

                    provider.setFocusable(editable);
                    provider.setCursorVisible(editable);
                    provider.setBackgroundColor(Color.TRANSPARENT);

                    year.setFocusable(editable);
                    year.setCursorVisible(editable);
                    year.setBackgroundColor(Color.TRANSPARENT);
                }
            } else {
                //Add click listener to common button.
                addSaveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewPager.getCurrentItem() == 0) {
                            Publications publications = new Publications();
                            List<String> collabsList = makeList(collabs.getText().toString());
                            List<String> mentorsList = makeList(mentors.getText().toString());
                            publications.setTitle(title.getText().toString());
                            publications.setReferenceNo(referenceNo.getText().toString());
                            publications.setJournal(journal.getText().toString());
                            publications.setCollabs(collabsList);
                            publications.setMentors(mentorsList);
                            publications.setDescription(description.getText().toString());
                            publications.setNumCitations(
                                    Integer.parseInt(numOfCitations.getText().toString()));
                            achievementsRecyclerViewAdapter.insertItem(publications);
                            title.setText("");
                            referenceNo.setText("");
                            journal.setText("");
                            collabs.setText("");
                            mentors.setText("");
                            description.setText("");
                            numOfCitations.setText("");
                        } else if (viewPager.getCurrentItem() == 1) {
                            Certificates certificates = new Certificates();
                            certificates.setTitle(titleCertificate.getText().toString());
                            certificates.setProvider(provider.getText().toString());
                            certificates.setYear(Integer.parseInt(year.getText().toString()));
                            achievementsRecyclerViewAdapter.insertItem(certificates);
                            titleCertificate.setText("");
                            provider.setText("");
                            year.setText("");
                        }
                    }
                });
            }
            return rootViewDialog;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        List<String> makeList(String input) {
            StringTokenizer stringTokenizer = new StringTokenizer(input, ",");
            List<String> output = new ArrayList<>();
            while (stringTokenizer.hasMoreElements()) {
                String s = stringTokenizer.nextToken();
                if (!s.equals(""))
                    output.add(s);
            }
            return output;
        }
    }
}
