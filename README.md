# ProfessorMatchingAndroid
Android front end part of Professor Matching Project

# Volley Singleton Pattern
Volley api implemented using singleton pattern.

# Student class
For storing details of a student obtained from json.

# JSON
To be implemented using GSON.
